<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider {

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot() {
        $this->registerPolicies();
        /////////////////// register all permissions
//        $modules = config('modules');
//        if ($modules) {
//            foreach ($modules as $key => $value) {
//                foreach ($value as $permission) {
//                    Gate::define($permission.'-'.$key, function ($user) use ($permission,$key){
//                        return in_array($permission.'-'.$key, json_decode($user->role->permissions));
//                    });
//                }
//            }
//        }
    }

}
