<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendContactUsToAdmin implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $row;

    public function __construct($row) {
        $this->row = $row;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {
            $row = $this->row;
            $toEmail = conf('contact_submit_email');
            \Mail::send('emails.contact-us', ['row' => $row], function ($mail) use ($row, $toEmail) {
                $subject = trans('email.Contact us submit form') . '-' . $row->name;
                $mail->to($toEmail, $row->name)
                        ->subject($subject);
            });
        } catch (\Exception $e) {
            \Log::error('Error: ' . $e->getMessage() . ', File: ' . $e->getFile() . ', Line:' . $e->getLine() . PHP_EOL);
        }
    }

}
