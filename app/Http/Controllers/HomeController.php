<?php

namespace App\Http\Controllers;

class HomeController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'home';
        $this->views = 'front.' . $this->module;
    }

    public function getIndex(\App\Models\Post $post, \App\Models\Course $course, \App\Models\User $user) {
        $data['page_title'] = trans('app.Home');
        $data['latestPosts'] = $post->getData()->active()->latest()->take(6)->get();
        $data['latestCourses'] = $course->getData()->active()->latest()->take(6)->get();
        $data['teachers'] = $user->getData()->active()->where('is_teacher',1)->latest()->take(4)->get();
        //$data['latestCourses'] = $course->getData()->active()->latest()->take(6)->get();
        return view($this->views . '.index', $data);
    }

}
