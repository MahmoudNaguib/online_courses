<?php

namespace App\Http\Controllers;

class ProfileController extends Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\User $model) {
        $this->module = 'profile';
        $this->model = $model;
        $this->rules = $model->rules;
    }
    public function getEdit() {
        $data['module'] = $this->module;
        $data['row'] = $this->model->findOrFail(auth()->user()->id);
        $data['page_title'] = trans('profile.Edit Profile');
        return view($this->module . '.edit', $data);
    }

    public function postEdit() {
        $row = $this->model->findOrFail(auth()->user()->id);
        unset($this->rules['role_id'], $this->rules['password']);
        $this->rules['email'].=',' . $row->id . ',id,deleted_at,NULL';
        $this->validate(request(), $this->rules);
        if ($row->update(request()->except(['password']))) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getChangePassword() {
        $data['module'] = $this->module;
        $data['row'] = $this->model->findOrFail(auth()->user()->id);
        $data['page_title'] = trans('profile.Change password');
        return view($this->module . '.change-password', $data);
    }

    public function postChangePassword() {
        $row = $this->model->findOrFail(auth()->user()->id);
        $this->validate(request(), ['password' => 'nullable|confirmed|min:8']);
        if ($row->update(request()->only(['password']))) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getLogout() {
        auth()->logout();
        session()->forget(['default_currency']);
        return back();
    }

}
