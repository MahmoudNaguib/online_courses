<?php

namespace App\Http\Controllers\Admin;
use Validator;

class UsersController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\User $model) {
        $this->module = 'admin/users';
        $this->title = trans('app.Users');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->where('is_default', '!=',1)->paginate(env('PAGE_LIMIT',15));
        return view($this->module . '.index', $data);
    }

    public function getCreate() {
        authorize('create-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title=>$this->module];
        $data['row'] = $this->model;
        $data['row']->language='en';
        return view($this->module . '.create', $data);
    }

    public function postCreate() {
        authorize('create-' . $this->module);
        $this->validate(request(), $this->rules);
        //////////////////////////////// check type
        if(request('type')=='admin')
            request()->request->add(['is_admin'=>1]);
        if(request('type')=='vendor')
            request()->request->add(['is_vendor'=>1]);
        ///////////////////////////////// Add Confirm Token
        request()->request->add(['confirm_token'=>request('email')]);
        ////////////////////////////////
        if($row = $this->model->create(request()->except(['password_confirmation', 'type']))) {
            flash()->success(trans('app.Created successfully'));
            return redirect(lang() . '/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
    }

    public function getEdit($id) {
        authorize('edit-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title=>$this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->module . '.edit', $data);
    }

    public function postEdit($id) {
        authorize('edit-' . $this->module);
        $this->rules['email'].=',' . $id . ',id,deleted_at,NULL';
        $this->rules['password'] = "nullable|confirmed|min:8";
        $this->validate(request(), $this->rules);
        $row = $this->model->findOrFail($id);
        //////////////////////////////// check type
        if(request('type')=='admin')
            request()->request->add(['is_admin'=>1]);
        if(request('type')=='vendor')
            request()->request->add(['is_vendor'=>1]);
        ////////////////////////////////
        if($row->update(request()->except(['password_confirmation', 'type']))) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView($id) {
        authorize('view-' . $this->module);
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title=>$this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->module . '.view', $data);
    }

    public function getDelete($id) {
        authorize('delete-' . $this->module);
        $row = $this->model->where('is_default', '!=',1)->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

    public function getExport() {
        authorize('view-' . $this->module);
        $rows = $this->model->getData()->get();
        if($rows->isEmpty()) {
            flash()->error(trans('app.There is no results to export'));
            return back();
        }
        $this->model->export($rows, $this->module);
    }
}