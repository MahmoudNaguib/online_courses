<?php

namespace App\Http\Controllers\Admin;

class CommentsController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Comment $model) {
        $this->module = 'admin/comments';
        $this->title = trans('app.Comments');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->latest()->paginate(env('PAGE_LIMIT', 15));
        $data['row'] = $this->model;
        return view($this->module . '.index', $data);
    }

    public function getDelete($id) {
        authorize('delete-' . $this->module);
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

    public function getExport() {
        authorize('view-' . $this->module);
        $rows = $this->model->getData()->get();
        if ($rows->isEmpty()) {
            flash()->error(trans('app.There is no results to export'));
            return back();
        }
        $this->model->export($rows, $this->module);
    }

}
