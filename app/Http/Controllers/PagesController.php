<?php

namespace App\Http\Controllers;

class PagesController extends \App\Http\Controllers\Controller
{

    public $module;

    public function __construct()
    {
        $this->module = 'pages';
        $this->views = 'front.' . $this->module;
    }
    public function getAbout()
    {
        $data['page_title'] = trans('app.About us');
        $data['meta_description'] = conf('about_meta_description');
        $data['meta_keywords'] = conf('about_meta_keywords');
        $data['image'] = conf('about_banner');
        $data['content'] = conf('about_text');
        return view($this->views . '.about', $data);
    }
    public function getContact()
    {
        $data['page_title'] = trans('app.Contact us');
        $data['meta_description'] = conf('contact_meta_description');
        $data['meta_keywords'] = conf('contact_meta_keywords');
        $data['image'] = conf('contact_banner');
        $data['content'] = conf('contact_text');
        return view($this->views . '.contact', $data);
    }
    public function getDetails($id, $slug = NULL)
    {
        $data['row'] = \App\Models\Page::active()->findOrFail($id);
        $data['page_title'] = $data['row']->title;
        $data['meta_description'] = $data['row']->meta_description;
        $data['meta_keywords'] = $data['row']->meta_keywords;
        $data['image'] = ($data['row']->image) ?: 'about_banner.jpg';
        $data['content'] = $data['row']->content;
        return view($this->views . '.details', $data);
    }
}
