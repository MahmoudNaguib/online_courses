<?php

namespace App\Http\Controllers;

class OrdersController extends \App\Http\Controllers\Controller {

    public function __construct(\App\Models\Order $model) {
        $this->middleware(['auth']);
        $this->module = 'orders';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
    }

    public function getAdd($id) {
        $data['row'] = \App\Models\Course::findOrFail($id);
        $orders = auth()->user()->orders->pluck('course_id')->toArray();
        if (in_array($id, $orders)) {
            flash()->success(trans('app.You already bought this course'));
            return redirect($data['row']->link);
        }
        $data['coupon'] = \App\Models\Coupon::where('code', request('code'))->where('course_id', $id)->where('expiry_date', '>=', date('Y-m-d'))->first();
        $data['page_title'] = trans('app.Order course') . ':' . $data['row']->title;
        return view($this->views . '.add', $data);
    }

    public function postAddCoupon($id) {
        $data['row'] = \App\Models\Course::findOrFail($id);
        $coupon = \App\Models\Coupon::where('code', request('code'))->where('course_id', $id)->where('expiry_date', '>=', date('Y-m-d'))->first();
        if (!$coupon) {
            flash()->error(trans('app.Invalid coupon'));
            return redirect(lang() . '/orders/add/' . $data['row']->id)->withInput();
        }
        else {
            flash()->success(trans('app.Coupon has been added'));
            return redirect(lang() . '/orders/add/' . $data['row']->id . '?code=' . $coupon->code)->withInput();
        }
    }

    public function postAdd($id) {
        $row = \App\Models\Course::findOrFail($id);
        $orders = auth()->user()->orders->pluck('course_id')->toArray();
        if (in_array($id, $orders)) {
            flash()->success(trans('app.You already bought this course'));
            return redirect($data['row']->link);
        }
        $coupon = \App\Models\Coupon::where('code', request('code'))->where('course_id', $id)->where('expiry_date', '>=', date('Y-m-d'))->first();
        $data = [
            'course_id' => $id,
            'price' => $row->price,
            'total' => $row->total,
            'discount' => $row->price - $row->total,
            'coupon_id' => @$coupon->id,
            'confirmed' => 1
        ];
        if (auth()->user()->orders()->create($data)) {
            flash()->success(trans('app.Your order has been created, Check your courses page'));
            return redirect($row->link);
        }
    }

}
