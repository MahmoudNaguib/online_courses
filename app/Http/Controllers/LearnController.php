<?php

namespace App\Http\Controllers;

class LearnController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Course $model) {
        $this->middleware(['auth']);
        $this->module = 'learn';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
    }

    public function getIndex(\App\Models\Course $course, \App\Models\Lecture $lecture) {
        $courseId = request('course_id');
        $data['row'] = $course->whereIn('id', auth()->user()->ordersList())->findOrFail($courseId);
        if (!request('lecture_id')) {
            $firstUnit = \App\Models\Unit::where('course_id', request('course_id'))->orderBy('order_field', 'ASC')->has('lectures')->first();
            $data['lec'] = \App\Models\Lecture::where('unit_id', $firstUnit->id)->orderBy('order_field', 'ASC')->first();
        } else {
            $data['lec'] = $lecture->where('course_id', $courseId)->findOrFail(request('lecture_id'));
        }
        $data['page_title'] = $data['row']->title;
        $data['lecture_id'] =$data['lec']->id;
        return view($this->views . '.lectures', $data);
    }

}
