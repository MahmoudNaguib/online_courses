<?php

namespace App\Http\Controllers;

class BioController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\User $model) {
        $this->module = 'bio';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
    }

    public function getDetails($id, $slug = NULL) {
        $data['row'] = $this->model->findOrFail($id);
        $data['page_title'] = $data['row']->name;
        return view($this->views . '.details', $data);
    }

}
