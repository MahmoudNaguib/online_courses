<?php

namespace App\Http\Controllers;

class PostsController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Post $model) {
        $this->module = 'posts';
        $this->views='front.'.$this->module;
        $this->model = $model;
    }

    public function getIndex() {
        $data['page_title'] = trans('app.Posts');
        $data['rows']=$this->model->getData()->active()->latest()->paginate(env('PAGE_LIMIT',10));
        return view($this->views . '.index', $data);
    }
    public function getDetails($id,$slug=NULL,  \App\Models\Post $post) {
        $data['row']=  $this->model->findOrFail($id);
        $data['page_title'] = $data['row']->title;
        $data['recentPosts']=$post->getData()->active()->where('id','!=',$id)->latest()->take(5)->get();
        return view($this->views . '.details', $data);
    }
}