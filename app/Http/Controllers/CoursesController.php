<?php

namespace App\Http\Controllers;

class CoursesController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Course $model) {
        $this->module = 'courses';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
        $this->middleware('auth')->only(['getMyCourses']);
    }

    public function getMyCourses(\App\Models\Course $course) {
        $data['page_title'] = trans('app.My courses');
        $data['rows'] = $course->getData()->whereIn('id',auth()->user()->ordersList())->active()->latest()->paginate(env('PAGE_LIMIT', 10));
        return view($this->views . '.my-courses', $data);
    }

    public function getIndex() {
        $data['page_title'] = trans('app.Courses');
        $data['rows'] = $this->model->getData()->active()->latest()->paginate(env('PAGE_LIMIT', 10));
        return view($this->views . '.index', $data);
    }

    public function getDetails($id, $slug = NULL, \App\Models\Course $post) {
        $data['row'] = $this->model->findOrFail($id);
        if (@auth()->user() && in_array($id,@auth()->user()->ordersList())) {
            return redirect('learn?course_id='.$id);
        }
        $data['page_title'] = $data['row']->title;
        $data['relatedCourses'] = $post->getData()->where('section_id', $data['row']->section_id)->active()->where('id', '!=', $id)->latest()->take(3)->get();
        return view($this->views . '.details', $data);
    }

}
