<?php

namespace App\Http\Controllers;

class FavouritesController extends \App\Http\Controllers\Controller {

    public function __construct(\App\Models\Favourite $model) {
        $this->middleware(['auth']);
        $this->module = 'favourites';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
    }

    public function getIndex(\App\Models\Course $course) {
        $favourites = auth()->user()->favourites->pluck('course_id')->toArray();
        $data['page_title'] = trans('app.Favourite courses');
        $data['rows'] = $course->getData()->whereIn('id', $favourites)->active()->latest()->paginate(env('PAGE_LIMIT', 10));
        return view($this->views . '.index', $data);
    }

    public function getAdd($id) {
        $favourites = auth()->user()->favourites->pluck('course_id')->toArray();
        if (in_array($id, $favourites)) {
            flash()->success(trans('app.This course already added to your favourites'));
        }
        else {
            auth()->user()->favourites()->create(['course_id' => $id]);
            flash()->success(trans('app.Added successfully'));
        }
        return back();
    }

}
