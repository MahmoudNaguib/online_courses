<?php

namespace App\Http\Controllers;

class CommentsController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Comment $model) {
        $this->module = 'comments';
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function postIndex() {
        switch (request('type')) {
            case 'post':
                $entity = \App\Models\Post::findOrFail(request('commentable_id'));
                request()->request->add(['commentable_type' => \App\Models\Post::class]);
                break;
            case 'course':
                $entity = \App\Models\Course::findOrFail(request('commentable_id'));
                request()->request->add(['commentable_type' => \App\Models\Course::class]);
                break;
            case 'user':
                $entity = \App\Models\User::findOrFail(request('commentable_id'));
                request()->request->add(['commentable_type' => \App\Models\User::class]);
                break;
        }
        $this->validate(request(), $this->rules);
        if ($entity) {
            if (\App\Models\Comment::create(request()->except(['type']))) {
                flash()->success(trans('app.Created successfully'));
                return back();
            }
        }
        flash()->error(trans('app.failed to save'));
    }

    public function postReply() {
        $comment = $this->model->findOrFail(request('reply_to'));
        request()->request->add([
            'commentable_id' => $comment->commentable_id,
            'commentable_type' => $comment->commentable_type,
        ]);
        $this->validate(request(), $this->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Created successfully'));
            return back();
        }
        flash()->error(trans('app.failed to save'));
    }

}
