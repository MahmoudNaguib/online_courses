<?php

namespace App\Http\Controllers;

use PhpParser\Node\Stmt\Catch_;

class ContactsController extends Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Contact $model) {
        $this->model = $model;
    }

    public function postIndex() {
        $this->validate(request(), $this->model->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Sent successfully'));
            return back();
        }
        flash()->error(trans('app.failed to save'));
    }

}
