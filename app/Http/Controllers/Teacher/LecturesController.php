<?php

namespace App\Http\Controllers\Teacher;

class LecturesController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Lecture $model) {
        $this->module = 'teacher/lectures';
        $this->title = trans('app.lectures');
        $this->model = $model;
        $this->rules = $model->rules;
        $this->coursesModule = 'teacher/courses';
    }

    public function getCreate() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model;
        $data['row']->is_active = 1;
        return view($this->module . '.create', $data);
    }

    public function postCreate() {
        $this->validate(request(), $this->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Created successfully'));
            return redirect(lang() . '/' . $this->coursesModule . '/view/' . $row->course_id);
        }
        flash()->error(trans('app.failed to save'));
    }

    public function getEdit($id) {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->module . '.edit', $data);
    }

    public function postEdit($id) {
        $row = $this->model->findOrFail($id);
        $this->validate(request(), $this->rules);
        if ($row->update(request()->all())) {
            flash(trans('app.Update successfully'))->success();
            return redirect(lang() . '/' . $this->coursesModule . '/view/' . $row->course_id);
        }
    }

    public function getDelete($id) {
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

}
