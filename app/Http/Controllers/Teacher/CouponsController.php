<?php

namespace App\Http\Controllers\Teacher;

class CouponsController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Coupon $model) {
        $this->module = 'teacher/coupons';
        $this->title = trans('app.Coupons');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->own()->latest()->paginate(env('PAGE_LIMIT', 10));
        $data['row'] = $this->model;
        return view($this->module . '.index', $data);
    }

    public function getCreate() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model;
        $data['row']->expiry_date = date('Y-m-d');
        $data['row']->total = 1;
        $data['row']->discount = 1;
        return view($this->module . '.create', $data);
    }

    public function postCreate() {
        $this->validate(request(), $this->rules);
        $coupon = \App\Models\Coupon::where('code', request('code'))->where('course_id', request('course_id'))->own()->first();
        if ($coupon) {
            flash()->error(trans('app.This coupon is already exist for the selected course'));
            return back()->withInput();
        }
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Created successfully'));
            return redirect(lang() . '/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
    }

    public function getEdit($id) {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->module . '.edit', $data);
    }

    public function postEdit($id) {
        $row = $this->model->findOrFail($id);
        $this->validate(request(), $this->rules);
        $coupon = \App\Models\Coupon::where('id','!=',$id)->where('code', request('code'))->where('course_id', request('course_id'))->own()->first();
        if ($coupon) {
            flash()->error(trans('app.This coupon is already exist for the selected course'));
            return back()->withInput();
        }
        if ($row->update(request()->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView($id) {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->module . '.view', $data);
    }

    public function getDelete($id) {
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

    public function getExport() {
        $rows = $this->model->getData()->own()->get();
        if ($rows->isEmpty()) {
            flash()->error(trans('app.There is no results to export'));
            return back();
        }
        $this->model->export($rows, $this->module);
    }

}
