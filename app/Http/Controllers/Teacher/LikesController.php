<?php

namespace App\Http\Controllers\Teacher;

class LikesController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Like $model) {
        $this->module = 'teacher/likes';
        $this->title = trans('app.Likes');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        //////////// check own
        $myCourses = \App\Models\Course::own()->pluck('id')->toArray();
        if (!in_array(request('course_id'), $myCourses)) {
            return abort(404);
        }
        ///////////////// end
        $data['rows'] = $this->model->getData()->latest()->paginate(env('PAGE_LIMIT', 15));
        $data['row'] = $this->model;
        return view($this->module . '.index', $data);
    }


}
