<?php

namespace App\Http\Controllers\Teacher;

class CoursesController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Course $model) {
        $this->module = 'teacher/courses';
        $this->commentsModule = 'teacher/comments';
        $this->likesModule = 'teacher/likes';
        $this->ratesModule = 'teacher/rates';
        $this->lecturesModule = 'teacher/lectures';
        $this->unitsModule = 'teacher/units';
        $this->title = trans('app.Courses');
        $this->model = $model;
        $this->rules = $model->rules;
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['commentsModule'] = $this->commentsModule;
        $data['likesModule'] = $this->likesModule;
        $data['ratesModule'] = $this->ratesModule;
        $data['lecturesModule'] = $this->lecturesModule;
        $data['unitsModule'] = $this->unitsModule;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->own()->latest()->paginate(env('PAGE_LIMIT', 10));
        $data['row'] = $this->model;
        return view($this->module . '.index', $data);
    }

    public function getCreate() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Create') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model;
        $data['row']->is_active = 1;
        $data['row']->has_certificate = 1;
        return view($this->module . '.create', $data);
    }

    public function postCreate() {
        foreach (langs() as $lang) {
            $slug[$lang] = slug(request('title.' . $lang));
        }
        request()->request->add(['slug' => $slug]);
        $this->validate(request(), $this->rules);
        if ($row = $this->model->create(request()->all())) {
            flash()->success(trans('app.Created successfully'));
            return redirect(lang() . '/' . $this->module);
        }
        flash()->error(trans('app.failed to save'));
    }

    public function getEdit($id) {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.Edit') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->module . '.edit', $data);
    }

    public function postEdit($id) {
        foreach (langs() as $lang) {
            $slug[$lang] = slug(request('title.' . $lang));
        }
        request()->request->add(['slug' => $slug]);
        $row = $this->model->findOrFail($id);
        $this->validate(request(), $this->rules);
        if ($row->update(request()->all())) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getView($id) {
        $data['module'] = $this->module;
        $data['commentsModule'] = $this->commentsModule;
        $data['likesModule'] = $this->likesModule;
        $data['ratesModule'] = $this->ratesModule;
        $data['lecturesModule'] = $this->lecturesModule;
        $data['unitsModule'] = $this->unitsModule;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->findOrFail($id);
        return view($this->module . '.view', $data);
    }

    public function getDelete($id) {
        $row = $this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

    public function getExport() {
        $rows = $this->model->getData()->own()->get();
        if ($rows->isEmpty()) {
            flash()->error(trans('app.There is no results to export'));
            return back();
        }
        $this->model->export($rows, $this->module);
    }

}
