<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title'];
    protected $table = "units";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'course_id'=>'required',
        'title' => 'required',
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public function getData() {
        return $this;
    }

    public function lectures() {
        return $this->hasMany(Lecture::class, 'unit_id')->orderBy('order_field', 'asc');
    }

    public function course() {
        return $this->belongsTo(Course::class, 'course_id')->withTrashed()->withDefault();
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Title'] = @$row->title;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

}
