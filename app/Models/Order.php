<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy;

    ///////////////////////////// has translation
    protected $table = "orders";
    protected $guarded = [
        'deleted_at',
        'logged_user',
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'course_id' => 'required',
        'price' => 'required|numeric',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'created_by')->withTrashed()->withDefault();
    }

    public function course() {
        return $this->belongsTo(Course::class, 'course_id')->withTrashed()->withDefault();
    }

    public function coupon() {
        return $this->belongsTo(Coupon::class, 'coupon_id')->withTrashed()->withDefault();
    }

    public function getCourses() {
        return \App\Models\Course::own()->pluck('title', 'id');
    }

    public function getData() {
        return $this->with(['course'])
                        ->when(request('course_id'), function($q) {
                            return $q->where('course_id', request('course_id'));
                        });
    }

    public function scopeOwn($query) {
        return $query->where('created_by', '=', auth()->user()->id);
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Course'] = $row->course->title;
                $object['Price'] = $row->price;
                $object['Discount'] = $row->discount;
                $object['Total'] = $row->total;
                $object['Coupon'] = $row->coupon->code;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

}
