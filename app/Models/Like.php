<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy;

    ///////////////////////////// has translation
    protected $table = "likes";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'likeable_id' => 'required',
        'likeable_type' => 'required',
    ];
    protected $appends = ['type'];

    public function getUsers() {
        return \App\Models\User::pluck('name', 'id');
    }

    public function getPosts() {
        return \App\Models\Post::pluck('title', 'id');
    }

    public function getCourses() {
        return \App\Models\Course::pluck('title', 'id');
    }

    public function likeable() {
        return $this->morphTo('likeable')->withTrashed()->withDefault();
    }

    public function getData() {
        return $this->with(['creator', 'likeable'])
                        ->when(request('post_id'), function($q) {
                            return $q->where('likeable_id', request('post_id'))->where('likeable_type', 'App\Models\Post');
                        })
                        ->when(request('course_id'), function($q) {
                            return $q->where('likeable_id', request('course_id'))->where('likeable_type', 'App\Models\Course');
                        });
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['type'] = $row->type;
                $object['title'] = $row->likeable->title;
                $object['Created by'] = $row->creator->name;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getTypeAttribute() {
        switch ($this->likeable_type) {
            case 'App\Models\Course':
                return 'Course';
            case 'App\Models\Post':
                return 'Post';
        }
    }

}
