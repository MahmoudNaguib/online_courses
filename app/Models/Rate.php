<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Rate extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy;

    ///////////////////////////// has translation
    protected $table = "rates";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'rateable_id' => 'required',
        'rateable_type' => 'required',
        'value' => 'required'
    ];
    protected $appends = ['type'];

    public function getUsers() {
        return \App\Models\User::pluck('name', 'id');
    }

    public function getTeacher() {
        return \App\Models\User::IsTeacher()->pluck('name', 'id');
    }

    public function getCourses() {
        return \App\Models\Course::pluck('title', 'id');
    }

    public function rateable() {
        return $this->morphTo('rateable')->withTrashed()->withDefault();
    }

    public function getData() {
        return $this->with(['creator', 'rateable'])
                        ->when(request('user_id'), function($q) {
                            return $q->where('rateable_id', request('user_id'))->where('rateable_type', 'App\Models\User');
                        })
                        ->when(request('course_id'), function($q) {
                            return $q->where('rateable_id', request('course_id'))->where('rateable_type', 'App\Models\Course');
                        });
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['type'] = $row->type;
                $object['title'] = $row->rateable->title;
                $object['Created by'] = $row->creator->name;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getTypeAttribute() {
        switch ($this->rateable_type) {
            case 'App\Models\Course':
                return 'Course';
            case 'App\Models\User':
                return 'Teacher';
        }
    }

}
