<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \Laravel\Scout\Searchable;

    ///////////////////////////// has translation
    public $translatable = ['content'];
    protected $table = "comments";
    protected $guarded = [
        'deleted_at',
        'logged_user',
        'type'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'commentable_id' => 'required',
        'commentable_type' => 'required',
        'content' => 'required',
    ];
    protected $appends = ['type'];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'content' => $this->content,
        ];
        return $array;
    }

    public function replies() {
        return $this->hasMany(Comment::class, 'reply_to');
    }

    public function getUsers() {
        return \App\Models\User::pluck('name', 'id');
    }

    public function getPosts() {
        return \App\Models\Post::pluck('title', 'id');
    }

    public function getCourses() {
        return \App\Models\Course::pluck('title', 'id');
    }

    public function commentable() {
        return $this->morphTo('commentable')->withTrashed()->withDefault();
    }

    public function getData() {
        return $this->with(['creator', 'commentable'])
                        ->when(request('post_id'), function($q) {
                            return $q->where('commentable_id', request('post_id'))->where('commentable_type', 'App\Models\Post');
                        })
                        ->when(request('course_id'), function($q) {
                            return $q->where('commentable_id', request('course_id'))->where('commentable_type', 'App\Models\Course');
                        });
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Post'] = $row->post->title;
                $object['Content'] = $row->content;
                $object['Created by'] = $row->creator->name;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 70);
    }

    public function getTypeAttribute() {
        switch ($this->commentable_type) {
            case 'App\Models\Course':
                return 'Course';
            case 'App\Models\Post':
                return 'Post';
        }
    }

}
