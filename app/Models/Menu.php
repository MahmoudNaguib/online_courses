<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Menu extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title'];
    protected $table = "menu";
    protected $guarded = [
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,240x180', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'title' => 'required',
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->title, ['small' => '300x211', 'large' => '720x540']);
                $data['image'] = $image;
                \DB::table('posts')->where('id', $row->id)->update($data);
            }
        });
    }

    public function top() {
        return $this->belongsTo(Menu::class, 'parent_id')->withTrashed()->withDefault();
    }

    public function childs() {
        return $this->hasMany(Menu::class, 'parent_id');
    }

    public function page() {
        return $this->belongsTo(Page::class, 'page_id')->withTrashed()->withDefault();
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function getData() {
        return $this->when(request('category_id'), function($q) {
                            return $q->where('category_id', request('category_id'));
                        })->when(request('title'), function($q) {
                            return $q->where('title', 'LIKE', '%' . request('title') . '%');
                        })
                        ->when(request('tags'), function($q) {
                            return $q->where('tags', 'LIKE', '%' . request('tags') . '%');
                        });
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Title'] = $row->title;
                $object['Order field'] = $row->order_field;
                $object['Is Active'] = ($row->is_active) ? trans('app.Yes') : trans('app.No');
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getLinkAttribute() {
        return app()->make("url")->to('/') . '/' . lang() . '/pages/details/' . $this->id . '/' . $this->slug;
    }

    public function getTitleLimitedAttribute() {
        return str_limit($this->title, 35);
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 60);
    }

}
