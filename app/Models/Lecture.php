<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Lecture extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title', 'content'];
    protected $table = "lectures";
    protected $guarded = [
        'deleted_at',
        'video',
        'attachment',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'course_id' => 'required',
        'unit_id' => 'required',
        'title' => 'required',
        'video' => 'nullable|max:100000',
        'attachment' => 'nullable|max:100000'
    ];
    protected static $attachFields = [
        'video' => [
            'path' => 'uploads'
        ],
        'attachment' => [
            'path' => 'uploads'
        ],
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
        ];
        return $array;
    }

    public function getData() {
        return $this->with(['unit', 'course'])
                        ->when(request('unit_id'), function($q) {
                            return $q->where('unit_id', request('unit_id'));
                        })
                        ->when(request('course_id'), function($q) {
                            return $q->where('course_id', request('course_id'));
                        });
    }

    public function getUnits($course_id) {
        return \App\Models\Unit::where('course_id', $course_id)->pluck('title', 'id');
    }

    public function course() {
        return $this->belongsTo(Course::class, 'course_id')->withTrashed()->withDefault();
    }

    public function unit() {
        return $this->belongsTo(Unit::class, 'unit_id')->withTrashed()->withDefault();
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Course'] = $row->course->title;
                $object['Unit'] = $row->unit->title;
                $object['Title'] = @$row->title;
                $object['Content'] = @$row->content;
                $object['Video'] = @$row->video;
                $object['Attachment'] = @$row->attachment;
                $object['url'] = @$row->url;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

}
