<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Favourite extends BaseModel {

    use SoftDeletes;

    ///////////////////////////// has translation
    protected $table = "favourites";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'course_id' => 'required',
        'user_id' => 'required'
    ];

    public function getData() {
        return $this;
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id')->withTrashed()->withDefault();
    }

    public function course() {
        return $this->belongsTo(Course::class, 'course_id')->withTrashed()->withDefault();
    }

}
