<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy;

    ///////////////////////////// has translation
    protected $table = "coupons";
    protected $guarded = [
        'deleted_at',
        'logged_user',
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'code' => 'required',
        'course_id' => 'required',
        'discount' => 'required|numeric|min:1',
        'expiry_date' => 'required|date|after_or_equal:today',
        'total' => 'required|integer|min:1'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'created_by')->withTrashed()->withDefault();
    }

    public function course() {
        return $this->belongsTo(Course::class, 'course_id')->withTrashed()->withDefault();
    }

    public function getCourses() {
        return \App\Models\Course::own()->pluck('title', 'id');
    }

    public function getData() {
        return $this->with(['course'])
                        ->when(request('course_id'), function($q) {
                            return $q->where('course_id', request('course_id'));
                        });
    }

    public function scopeOwn($query) {
        return $query->where('created_by', '=', auth()->user()->id);
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Course'] = $row->course->title;
                $object['Coupon'] = $row->coupon;
                $object['Discount'] = $row->discount;
                $object['Total'] = $row->total;
                $object['Expiry date'] = $row->expiry_date;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

}
