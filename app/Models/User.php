<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    protected $table = "users";
    protected $guarded = [
        'deleted_at',
        'logged_user',
        'image',
        'teams',
    ];
    protected $hidden = [
        'password',
        'remember_token',
        'confirm_token',
        'confirmed',
        'is_active',
        'is_admin',
        'created_by',
        'updated_at',
        'deleted_at',
        'language'
    ];
    protected $appends = ['is_admin', 'is_super_admin', 'title'];
    public $translatable = ['bio', 'meta_description', 'meta_keywords'];
    ///////////////////////////// has attach
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,240x180', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'mobile' => 'required|mobile',
        'password' => "required|confirmed|min:8",
        'image' => 'nullable|image|max:4000'
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'mobile' => $this->mobile,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->title);
                $data['image'] = $image;
                \DB::table('users')->where('id', $row->id)->update($data);
            }
            \App\Jobs\SendRegisterEMail::dispatch($row);
        });
    }

    public function getIsAdminAttribute() {
        return ($this->role_id);
    }

    public function getIsSuperAdminAttribute() {
        return ($this->role_id == 1);
    }

    public function getTitleAttribute() {
        return $this->name;
    }

    public function notifications() {
        return $this->hasMany(Notification::class, 'to_id');
    }

    public function role() {
        return $this->belongsTo(Role::class, 'role_id')->withTrashed()->withDefault();
    }

    public function tokens() {
        return $this->hasMany(Token::class);
    }

    public function favourites() {
        return $this->hasMany(Favourite::class)->with(['course']);
    }

    public function orders() {
        return $this->hasMany(Order::class, 'created_by')->with(['course']);
    }
    public function ordersList() {
        return $this->hasMany(Order::class, 'created_by')->pluck('course_id')->toArray();
    }

    public function courses() {
        return $this->hasMany(Course::class, 'created_by');
    }

    public function getGenders() {
        return \App\Models\Option::where('type', 'gender')->pluck('title', 'id');
    }

    public function getRoles() {
        return \App\Models\Role::where('id', '!=', 1)->pluck('title', 'id');
    }

    public function setPasswordAttribute($value) {
        if (trim($value)) {
            $this->attributes['password'] = bcrypt(trim($value));
        }
    }

    public function setConfirmTokenAttribute($value) {
        if (trim($value)) {
            $this->attributes['confirm_token'] = md5(trim($value)) . md5(time());
        }
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1)->where('confirmed', 1);
    }

    public function scopeIsTeacher($query) {
        return $query->where('is_teacher', '=', 1)->where('confirmed', 1);
    }

    public function getData() {
        return $this->with(['role', 'creator']);
    }

    public function rates() {
        return $this->morphMany(Rate::class, 'rateable');
    }

    public function getrateAttribute() {
        if ($this->rates->count() > 0)
            return floor($this->rates->sum('value') / $this->rates->count());
        else
            return 0;
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Name'] = $row->name;
                $object['Email'] = $row->email;
                $object['Mobile'] = $row->mobile;
                $object['Is Admin'] = ($row->is_admin) ? 'Yes' : 'No';
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getBioLimitedAttribute() {
        return str_limit(strip_tags($this->bio), 60);
    }

    public function getLinkAttribute() {
        $slug = slug($this->name);
        return app()->make("url")->to('/') . '/' . lang() . '/bio/details/' . $this->id . '/' . $slug;
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }

}
