<?php

namespace App\Models\Traits;

use DB;

trait EmailNotify {

    public static function bootEmailNotify() {
        static::created(function ($model) {
          if($model->email_notify){
            \App\Jobs\SendNotificationMail::dispatch($model); 
          }
        });
    }

}
