<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {

    public function getData() {
        return $this;
    }

    public function getCountries() {
        return \App\Models\Country::get(['id', 'iso', 'title'])->pluck('name', 'id');
    }
    public function getPages() {
        return \App\Models\Page::pluck('title', 'id');
    }
    public function getMainMenuItems() {
        return \App\Models\Menu::where('parent_id',NULL)->pluck('title', 'id');
    }
    public function getCurrencies() {
        return \App\Models\Currency::pluck('title', 'id');
    }

    public function getGenders() {
        return [
            'm'=>trans('app.Male'),
            'f'=>trans('app.Female')
        ];
    }

    public function getOptions($type=NULL) {
        $query=\App\Models\Option::active();
        if($type) {
            $query=$query->where('type', $type);
        }
        return $query->pluck('title', 'id')->toArray();
    }
}