<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title', 'tags', 'slug', 'content', 'meta_description', 'meta_keywords'];
    protected $table = "courses";
    protected $guarded = [
        'deleted_at',
        'logged_user',
        'image',
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,300x211', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
        'video' => [
            'path' => 'uploads',
        ],
    ];
    public $rules = [
        'section_id' => 'required',
        'price' => 'required|numeric',
        'title' => 'required',
        'level_id' => 'required',
        'language_id' => 'required',
        'content' => 'required',
        'image' => 'nullable|image|max:4000'
    ];

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->title, ['small' => '240x180', 'large' => '720x540']);
                $data['image'] = $image;
                \DB::table('courses')->where('id', $row->id)->update($data);
            }
        });
    }

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'content' => $this->content,
            'meta_description' => $this->meta_description,
            'meta_keywords' => $this->meta_keywords,
            'tags' => $this->tags
        ];
        return $array;
    }

    public function getSectionsWithParents() {
        return \App\Models\Section::with(['main'])->active()->where('parent_id', '!=', NULL)->get(['id', 'parent_id', 'title'])->groupBy('main.title')
                        ->map(function ($item) {
                            return $item->pluck('title', 'id');
                        });
    }

    public function section() {
        return $this->belongsTo(Section::class, 'section_id')->withTrashed()->withDefault();
    }

    public function language() {
        return $this->belongsTo(Option::class, 'language_id')->withTrashed()->withDefault();
    }

    public function level() {
        return $this->belongsTo(Option::class, 'level_id')->withTrashed()->withDefault();
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function rates() {
        return $this->morphMany(Rate::class, 'rateable');
    }

    public function units() {
        return $this->hasMany(Unit::class)->orderBy('order_field', 'asc');
    }

    public function lectures() {
        return $this->hasMany(Lecture::class)->orderBy('order_field', 'Asc');
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function scopeOwn($query) {
        return $query->where('created_by', '=', auth()->user()->id);
    }

    public function getData() {
        return $this->with(['creator', 'language', 'level', 'section', 'comments', 'likes'])
                        ->when(request('section_id'), function($q) {
                            return $q->where('section_id', request('section_id'));
                        })
                        ->when(request('q'), function($q) {
                            return $q->where('title', 'LIKE', '%' . request('q') . '%');
                        })
                        ->when(request('tags'), function($q) {
                            return $q->where('tags', 'LIKE', '%' . request('tags') . '%');
                        });
    }

    public function getLanguages() {
        return \App\Models\Option::where('type', 'languages')->pluck('title', 'id');
    }

    public function getlevels() {
        return \App\Models\Option::where('type', 'levels')->pluck('title', 'id');
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Title'] = @$row->title;
                $object['Section'] = @$row->section->title;
                $object['Level'] = @$row->level->title;
                $object['language'] = @$row->language->title;
                $object['About'] = @$row->about;
                $object['Description'] = @$row->description;
                $object['meta_description'] = @$row->meta_description;
                $object['meta_keywords'] = @$row->meta_keywords;
                $object['tags'] = @$row->tags;
                $object['Created at'] = $row->created_at;
                $object['Creator'] = $row->creator->name;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getLinkAttribute() {
        return app()->make("url")->to('/') . '/' . lang() . '/courses/details/' . $this->id . '/' . $this->slug;
    }

    public function getOrderButtonAttribute() {
        if (@auth()->user() && @auth()->user()->ordersList() && in_array($this->id,@auth()->user()->ordersList())) {
            $link=app()->make("url")->to('/') . '/' . lang() . '/learn?course_id=' . $this->id;
            return '<a href="'.$link.'" class="btn btn-theme btn-secondary">'.trans('app.Learn').'</a>';
        }
        else {
            $link=app()->make("url")->to('/') . '/' . lang() . '/orders/add/' . $this->id;
            return '<a href="'.$link.'" class="btn btn-theme btn-secondary">'.trans('app.Buy').'</a>';
        }
    }

    public function gettotalAttribute() {
        if (request('code')) {
            $coupon = \App\Models\Coupon::where('code', request('code'))->where('course_id', $this->id)->where('expiry_date', '>=', date('Y-m-d'))->first();
            if ($coupon) {
                $total = $this->price - ($this->price * $coupon->discount / 100);
                return $total;
            }
        }
        return $this->price;
    }

    public function getrateAttribute() {
        if ($this->rates->count() > 0)
            return floor($this->rates->sum('value') / $this->rates->count());
        else
            return 0;
    }

    public function getTitleLimitedAttribute() {
        return str_limit($this->title, 35);
    }

    public function getContentLimitedAttribute() {
        return str_replace('&nbsp;','',str_limit(strip_tags($this->content), 60));
    }

}
