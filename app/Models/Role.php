<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy;

    protected $table = "roles";
    protected $guarded = [
        'deleted_at',
        'logged_user',
        'parents'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = ['title' => 'required|unique:roles,title', 'permissions' => 'required'];

    public function getPermissionsAttribute($value) {
        return json_decode($value);
    }

    public function setPermissionsAttribute($value) {
        if ($value) {
            $this->attributes['permissions'] = json_encode($value);
        }
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Title'] = $row->title;
                $object['Permission'] = @implode(', ', (@$row->permissions)? : []);
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

}
