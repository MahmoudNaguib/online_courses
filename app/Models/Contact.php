<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \Laravel\Scout\Searchable;

    protected $table = "contacts";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'email' => 'required|email',
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'email' => $this->email,
        ];
        return $array;
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['ID'] = $row->id;
                $object['Email'] = $row->email;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

}
