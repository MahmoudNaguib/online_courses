<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\EmailNotify;

    protected $table = "notifications";
    protected $guarded = [
        'deleted_at',
        'logged_user',
    ];
    protected $hidden = [
        'deleted_at',
    ];

    public function from() {
        return $this->belongsTo(User::class, 'from_id')->withTrashed()->withDefault();
    }

    public function to() {
        return $this->belongsTo(User::class, 'to_id')->withTrashed()->withDefault();
    }

    public function scopeUnreaded($query) {
        return $query->where('seen_at', NULl);
    }

    public function sendNotification($request) {
        return $this->create($request);
    }

    public function getData() {
        $user = request('logged_user');
        if (!$user) {
            $user = auth()->user();
        }
        return $this->where('to_id', $user->id);
    }

}
