<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

    <head>
        @include('admin.partials.meta')
        @include('admin.partials.css')
        @stack('css')
    </head>

    <body>
        <div class="slim-header">
            <div class="container">
                <div class="slim-header-left">
                    @include('admin.partials.logo')
                    @include('admin.partials.search')
                </div>
                <!-- slim-header-left -->
                <div class="slim-header-right">
                    @include('admin.partials.notifications')
                    @include('admin.partials.langSwitch')
                    @include('admin.partials.user_navigation')
                </div>
                <!-- header-right -->
            </div>
            <!-- container -->
        </div>
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-8 mx-auto">
                    @include('admin.partials.flash_messages')
                    @yield('content')
                </div>
            </div>
        </div>


        <!-- d-flex -->
        <!-- signin-wrapper -->
        @include('admin.partials.js') 
        @stack('js')
    </body>

</html>
