<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    <head>
        @include('teacher.partials.meta')
        @include('teacher.partials.css') @stack('css')
    </head>
    
    <body>
        <div class="slim-header">
            <div class="container">
                <div class="slim-header-left">
                    @include('teacher.partials.logo')
                    @include('teacher.partials.search')
                    <!-- search-box -->
                </div>
                <!-- slim-header-left -->
                <div class="slim-header-right">
                    @include('teacher.partials.notifications')
                    @include('teacher.partials.langSwitch')
                    @include('teacher.partials.user_navigation')
                </div>
                <!-- header-right -->
            </div>
            <!-- container -->
        </div>
        <!-- slim-header -->
        @include('teacher.partials.navigation')
        <!-- slim-navbar -->
        <div class="slim-mainpanel">
            <div class="container">
                @include('teacher.partials.breadcrumb')
                @include('teacher.partials.flash_messages')
                <!-- section-wrapper -->
                @yield('content')
                <!-- section-wrapper -->
            </div>
            <!-- container -->
        </div>
        <!-- slim-mainpanel -->
        <!-- slim-footer -->
        @include('teacher.partials.footer')
        @include('teacher.partials.js') @stack('js')
    </body>

</html>
