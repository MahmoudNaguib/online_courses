@include('form.boolean',['name'=>'has_certificate','attributes'=>['label'=>trans('courses.Has Certificate')]])

@include('form.select',['name'=>'section_id','options'=>$row->getSectionsWithParents(),'attributes'=>['class'=>'form-control','label'=>trans('courses.Section'),'placeholder'=>trans('courses.Select Section'),'required'=>1]])

@include('form.input',['name'=>'price','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('courses.Price'),'placeholder'=>trans('courses.Price'),'step'=>'0.01','required'=>1,'min'=>0,'pattern'=>'^\d*(\.\d{0,2})?$']])


@include('form.select',['name'=>'level_id','options'=>$row->getLevels(),'attributes'=>['class'=>'form-control','label'=>trans('courses.Level'),'placeholder'=>trans('courses.Select level'),'required'=>1]])

@include('form.select',['name'=>'language_id','options'=>$row->getLanguages(),'attributes'=>['class'=>'form-control','label'=>trans('courses.Language'),'placeholder'=>trans('courses.Select language'),'required'=>1]])

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('courses.Title').' '.$lang,'placeholder'=>trans('courses.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control editor','label'=>trans('courses.Content').' '.$lang,'placeholder'=>trans('courses.Content')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'content['.$lang.']','value'=>$row->getTranslation('content',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control tags','label'=>trans('courses.Tags').' '.$lang,'placeholder'=>trans('courses.Tags')];
@endphp

@include('form.input',['name'=>'tags['.$lang.']','value'=>$row->getTranslation('tags',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('courses.Meta description').' '.$lang,'placeholder'=>trans('courses.Meta description')];
@endphp

@include('form.input',['name'=>'meta_description['.$lang.']','value'=>$row->getTranslation('meta_description',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control tags','label'=>trans('courses.Meta keywords').' '.$lang,'placeholder'=>trans('courses.Meta keywords')];
@endphp

@include('form.input',['name'=>'meta_keywords['.$lang.']','value'=>$row->getTranslation('meta_keywords',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@include('form.input',['name'=>'facebook_pixel','type'=>'textarea','attributes'=>['class'=>'form-control','label'=>trans('courses.Facebook pixel'),'placeholder'=>trans('courses.Facebook pixel')]])

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('courses.Image'),'placeholder'=>trans('courses.Image'),'required'=>($row->id)?false:true]])


@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('courses.Is active')]])

@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
            
            var content_en=$('textarea[name="content[en]"]').val();
            @foreach(langs() as $lang)
                if($('textarea[name="content[{{$lang}}]"]').val()=='')
                    $('textarea[name="content[{{$lang}}]"]').val(about_en);
            @endforeach
            
            var tags_en=$('input[name="tags[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="tags[{{$lang}}]"]').val()=='')
                    $('input[name="tags[{{$lang}}]"]').val(tags_en);
            @endforeach
            
            var meta_description_en=$('textarea[name="meta_description[en]"]').val();
            @foreach(langs() as $lang)
                if($('textarea[name="meta_description[{{$lang}}]"]').val()=='')
                    $('textarea[name="meta_description[{{$lang}}]"]').val(meta_description_en);
            @endforeach
            
            var meta_keywords_en=$('input[name="meta_keywords[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="meta_keywords[{{$lang}}]"]').val()=='')
                    $('input[name="meta_keywords[{{$lang}}]"]').val(meta_keywords_en);
            @endforeach

        });
});
</script>
@endpush
