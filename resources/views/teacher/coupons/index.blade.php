@extends('teacher.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    <a href="{{$module}}/create" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('app.Create')}}
    </a>
    <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
    </a>
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @include($module.'.partials.filters')
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('coupons.ID')}} </th>
                    <th class="wd-20p">{{trans('coupons.Course')}} </th>
                    <th class="wd-10p">{{trans('coupons.Code')}} </th>
                    <th class="wd-10p">{{trans('coupons.Discount')}}(%)</th>
                    <th class="wd-10p">{{trans('coupons.Expiry date')}} </th>
                    <th class="wd-10p">{{trans('coupons.Total')}} </th>
                    <th class="wd-15p">{{trans('coupons.Created at')}}</th>
                    <th class="wd-20p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->course->title}}</td>
                    <td class="center">{{$row->code}}</td>
                    <td class="center">{{$row->discount}}</td>
                    <td class="center">{{$row->expiry_date}}</td>
                    <td class="center">{{$row->total}}</td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">
                        <a class="btn-xs" href="{{$module}}/edit/{{$row->id}}" title="{{trans('coupons.Edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('coupons.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>

                        <a class="btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('coupons.Delete')}}" data-confirm="{{trans('coupons.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->appends(['course_id'=>request('course_id')])->render() !!}
    </div>
    @else
    {{trans("coupons.There is no results")}}
    @endif
</div>
@endsection
