@include('form.select',['name'=>'course_id','options'=>$row->getCourses(),'attributes'=>['class'=>'form-control','label'=>trans('coupons.Course'),'placeholder'=>trans('coupons.Select Course'),'required'=>1]])

@include('form.input',['name'=>'code','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('coupons.Code'),'placeholder'=>trans('coupons.Code'),'required'=>1]])


@include('form.input',['name'=>'discount','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('coupons.Discount').'(%)','placeholder'=>trans('coupons.Discount').'(%)','step'=>'0.01','required'=>1,'min'=>0,'pattern'=>'^\d*(\.\d{0,2})?$']])

@include('form.input',['name'=>'expiry_date','type'=>'text','attributes'=>['class'=>'form-control datepicker','label'=>trans('coupons.Expiry date'),'placeholder'=>trans('coupons.Expiry date'),'required'=>1]])

@include('form.input',['name'=>'total','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('coupons.Total'),'placeholder'=>trans('coupons.Total'),'required'=>1,'min'=>1]])
