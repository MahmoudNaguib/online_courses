@extends('teacher.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('edit-'.$module))
    <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('coupons.Edit')}}
    </a><br>
    @endif
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">

            <tr>
                <td width="25%" class="align-left">{{trans('coupons.Course')}}</td>
                <td width="75%" class="align-left">{{@$row->course->title}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('coupons.Code')}}</td>
                <td width="75%" class="align-left">{{@$row->code}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('coupons.Discount')}}</td>
                <td width="75%" class="align-left">{{@$row->discount}}  (%)</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('coupons.Expiry date')}}</td>
                <td width="75%" class="align-left">{{@$row->expiry_date}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('coupons.Total')}}</td>
                <td width="75%" class="align-left">{{@$row->total}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('categories.Created at')}}</td>
                <td width="75%" class="align-left">{{@$row->created_at}}</td>
            </tr>
        </table>
    </div>
</div>
@endsection
