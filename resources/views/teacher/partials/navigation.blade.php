<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{(request()->is(lang()))?"active":""}}">
                <a class="nav-link" href="{{app()->make("url")->to('/')}}/{{lang()}}/teacher">
                    <i class="icon ion-ios-pie-outline"></i>
                    <span>{{trans('navigation.Dashboard')}}</span>
                </a>
            </li>

            <li class="nav-item {{(request()->is('*/courses*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/teacher/courses">
                    <i class="icon fa ion-ios-compose"></i> 
                    <span>{{trans('navigation.Courses')}}</span>
                </a>
            </li>
            <li class="nav-item {{(request()->is('*/coupons*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/teacher/coupons">
                    <i class="icon fa ion-ios-compose"></i> 
                    <span>{{trans('navigation.Coupons')}}</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- container -->
</div>