@extends('teacher.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('courses.Edit')}}
    </a><br>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Total Comments')}} </td>
                <td width="75%" class="align-left">
                    <a class="btn-xs" href="{{$commentsModule}}?post_id={{$row->id}}" title="{{trans('courses.Comments')}}" target="_blank">
                        {{$row->comments->count()}}  <i class="icon fa fa-comment"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Total likes')}}</td>
                <td width="75%" class="align-left">
                    <a class="btn-xs" href="{{$likesModule}}?post_id={{$row->id}}" title="{{trans('courses.Likes')}}"  target="_blank">
                        {{$row->likes->count()}} <i class="icon fa fa-thumbs-up"></i>
                    </a>
                </td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Has certificate')}}</td>
                <td width="75%" class="align-left"><img src="backend/img/{{($row->has_certificate)?'check.png':'close.png'}}"></td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Section')}}</td>
                <td width="75%" class="align-left">{{$row->section->title}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Level')}}</td>
                <td width="75%" class="align-left">{{$row->level->title}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Language')}}</td>
                <td width="75%" class="align-left">{{$row->language->title}}</td>
            </tr>
            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Title')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('title',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Slug')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('slug',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Content')}} {{$lang}}</td>
                <td width="75%" class="align-left">{!! @$row->getTranslation('content',$lang) !!}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Tags')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('tags',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Meta description')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('meta_description',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Meta keywords')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('meta_keywords',$lang)}}</td>
            </tr>
            @endforeach

            <tr>
                <td width="25%" class="align-left">{{trans('courses.Image')}}</td>
                <td width="75%" class="align-left">{!! image($row->image,'small') !!}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Is active')}}</td>
                <td width="75%" class="align-left"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('courses.Created by')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>

        </table>
    </div>
</div>
@endsection
