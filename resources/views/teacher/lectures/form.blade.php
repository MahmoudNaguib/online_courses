@php
    $course_id=(request('course_id'))?:$row->course_id;
@endphp
<input type="hidden" name="course_id" value="{{$course_id}}">
@php
    $lastlecture=\App\Models\Lecture::where('course_id',$course_id)->orderBy('order_field','desc')->first();
@endphp
<input type="hidden" name="order_field" value="{{($lastlecture)?($lastlecture->order_field+1):1}}">


@include('form.select',['name'=>'unit_id','options'=>$row->getUnits($course_id),'attributes'=>['class'=>'form-control','label'=>trans('lectures.Unit'),'placeholder'=>trans('lectures.Select Unit'),'required'=>1]])

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('lectures.Title').' '.$lang,'placeholder'=>trans('lectures.Title')];
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control editor','label'=>trans('lectures.Content').' '.$lang,'placeholder'=>trans('lectures.Content')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'content['.$lang.']','value'=>$row->getTranslation('content',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach

@include('form.file',['name'=>'video','attributes'=>['file_type'=>'video','class'=>'form-control attach custom-file-input','label'=>trans('lectures.Video'),'placeholder'=>trans('lectures.Video'),'required'=>($row->id)?false:true]])


@include('form.file',['name'=>'attachment','attributes'=>['file_type'=>'attachment','class'=>'form-control attach custom-file-input','label'=>trans('lectures.Attachment'),'placeholder'=>trans('lectures.Attachment')]])


@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('lectures.Is active')]])


@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
            
            var content_en=$('textarea[name="content[en]"]').val();
            @foreach(langs() as $lang)
                if($('textarea[name="content[{{$lang}}]"]').val()=='')
                    $('textarea[name="content[{{$lang}}]"]').val(about_en);
            @endforeach
        });
});
</script>
@endpush
