@extends('teacher.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('rates.ID')}} </th>
                    <th class="wd-5p">{{trans('rates.Type')}} </th>
                    <th class="wd-15p">{{trans('rates.Title')}}</th>
                    <th class="wd-5p">{{trans('rates.Value')}} </th>
                    <th class="wd-10p">{{trans('rates.Created by')}} </th>
                    <th class="wd-15p">{{trans('rates.Created at')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->type}}</td>
                    <td class="center">
                        @if($row->type=='Course')
                        {{$row->rateable->title}}
                        @else
                        {{$row->rateable->title}}
                        @endif
                    </td>
                    <td class="center">{{$row->value}}</td>
                    <td class="center">{{@$row->creator->name}}</td>
                    <td class="center">{{$row->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->appends(['user_id'=>request('user_id'),'post_id'=>request('post_id')])->render() !!}
    </div>
    @else
    {{trans("rates.There is no results")}}
    @endif
</div>
@endsection
