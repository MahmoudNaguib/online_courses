@include('form.select',['name'=>'parent_id','options'=>$row->getParents(),'attributes'=>['class'=>'form-control','label'=>trans('sections.Parent'),'placeholder'=>trans('posts.Select Parent')]])

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('sections.Title').' '.$lang,'placeholder'=>trans('sections.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach


@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('sections.Image'),'placeholder'=>trans('sections.Image')]])

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('sections.Is active')]])


@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
        });
});
</script>
@endpush
