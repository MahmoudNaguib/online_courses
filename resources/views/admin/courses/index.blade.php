@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('create-'.$module))
    <a href="{{$module}}/create" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('app.Create')}}
    </a>
    @endif
    @if(can('view-'.$module))
    <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
    </a>
    @endif
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('view-'.$module))
    @include($module.'.partials.filters')
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('courses.ID')}} </th>
                    <th class="wd-10p">{{trans('courses.Section')}} </th>
                    <th class="wd-20p">{{trans('courses.Title')}} </th>
                    <th class="wd-5p"><i class="icon fa fa-comment"></i></th>
                    <th class="wd-5p"><i class="icon fa fa-thumbs-up"></i></th>
                    <th class="wd-5p"><i class="icon fa fa-plus"></i></th>
                    <th class="wd-5p">{{trans('courses.Views count')}} </th>
                    <th class="wd-5p">{{trans('courses.Active')}} </th>
                    <th class="wd-5p">{{trans('courses.Price')}} ({{trans('courses.EGP')}})</th>
                    <th class="wd-10p">{{trans('courses.Created by')}} </th>
                    <th class="wd-10p">{{trans('courses.Created at')}}</th>
                    <th class="wd-20p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->section->title}}</td>
                    <td class="center">
                        <a href="{{$module}}/view/{{$row->id}}" title="{{trans('courses.View')}}">{{str_limit($row->title,50)}}
                        </a>
                    </td>
                    <td class="center">{{$row->comments->count()}}</td>
                    <td class="center">{{$row->likes->count()}}</td>
                    <td class="center">{{$row->rates->count()}}</td>
                    <td class="center">{{$row->views}}</td>
                    <td class="center"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
                    <td class="center">
                        <span class="price">
                            {{$row->price}}
                        </span>
                    </td>
                    <td class="center">{{@$row->creator->name}}</td>
                    <td class="center">{{str_limit($row->created_at,10,false)}}</td>
                    <td class="center">
                        @if(can('view-'.$commentsModule))
                        <a class="btn-xs" href="{{$commentsModule}}?course_id={{$row->id}}" title="{{trans('courses.Comments')}}" target="_blank">
                            <i class="icon fa fa-comment"></i>
                        </a>
                        @endif

                        @if(can('view-'.$likesModule))
                        <a class="btn-xs" href="{{$likesModule}}?course_id={{$row->id}}" title="{{trans('courses.Likes')}}"  target="_blank">
                            <i class="icon fa fa-thumbs-up"></i>
                        </a>
                        @endif

                        @if(can('view-'.$ratesModule))
                        <a class="btn-xs" href="{{$ratesModule}}?course_id={{$row->id}}" title="{{trans('courses.Rates')}}"  target="_blank">
                            <i class="icon fa fa-plus"></i>
                        </a>
                        @endif

                        @if(can('edit-'.$module))
                        <a class="btn-xs" href="{{$module}}/edit/{{$row->id}}" title="{{trans('courses.Edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endif

                        @if(can('view-'.$module))
                        <a class="btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('courses.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        @endif

                        @if(can('delete-'.$module))
                        <a class="btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('courses.Delete')}}" data-confirm="{{trans('courses.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->appends(['created_by'=>request('created_by'),'section_id'=>request('section_id')])->render() !!}
    </div>
    @else
    {{trans("courses.There is no results")}}
    @endif
    @endif
</div>
@endsection
