@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('create-'.$unitsModule))
    <a href="{{$unitsModule}}/create?course_id={{$row->id}}" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('app.Create')}} {{trans('app.Unit')}}
    </a>
    @endif    
    @if(can('create-'.$lecturesModule))
    <a href="{{$lecturesModule}}/create?course_id={{$row->id}}" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('app.Create')}} {{trans('app.lecture')}}
    </a>
    @endif
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                @if($row->lectures)
                <ol class='sortable vertical'>
                    @foreach ($row->units as $unit)
                    <li data-id='{{$unit->id}}' data-name='{{$unit->title}}'>
                        <b>{{$unit->title}}</b>
                        @if($unit->lectures)
                        @if(can('edit-'.$unitsModule))
                        <a href="{{$unitsModule}}/edit/{{$unit->id}}" title="{{trans('courses.Edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endif

                        @if(can('delete-'.$unitsModule))
                        <a href="{{$unitsModule}}/delete/{{$unit->id}}" title="{{trans('courses.Delete')}}" data-confirm="{{trans('courses.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        @endif

                        <ol>
                            @foreach($unit->lectures as $lecture)
                            <li data-id='{{$lecture->id}}' data-name='lectures'>
                                {{$lecture->title}}

                                @if(can('edit-'.$lecturesModule))
                                <a href="{{$lecturesModule}}/edit/{{$lecture->id}}" title="{{trans('courses.Edit')}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @endif

                                @if(can('delete-'.$lecturesModule))
                                <a href="{{$lecturesModule}}/delete/{{$lecture->id}}" title="{{trans('courses.Delete')}}" data-confirm="{{trans('courses.Are you sure you want to delete this item')}}?">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                                @endif
                            </li>
                            @endforeach
                        </ol>
                        @endif
                    </li>
                    @endforeach
                </ol>
                @endif
            </div>
            <div class="col-md-4">
                <div class="card card-blog mg-b-20">
                    <figure class="card-item-img">
                        {!! image($row->image,'large',['class'=>'img-fluid']) !!}
                    </figure>
                    <div class="card-body">
                        <p class="blog-category">
                            {{$row->created_at}} 
                            @if(can('edit-'.$module))
                            <a href="{{$module}}/edit/{{$row->id}}">
                                <i class="fa fa-edit"></i> {{trans('courses.Edit')}}
                            </a>
                            @endif
                            <span class="price">
                                {{$row->price}} <span class="currency">{{trans('courses.EGP')}}</span>
                            </span>
                        </p>
                        <p class="blog-category">
                            
                            @if(can('view-'.$ratesModule))
                            <a class="btn-xs" href="{{$ratesModule}}?course_id={{$row->id}}" title="{{trans('courses.Rates')}}" target="_blank">{{$row->rates->count()}} <i class="icon fa fa-plus"></i>
                            </a>
                            @endif

                            @if(can('view-'.$commentsModule))
                            <a class="btn-xs" href="{{$commentsModule}}?course_id={{$row->id}}" title="{{trans('courses.Comments')}}" target="_blank">{{$row->comments->count()}} <i class="icon fa fa-comment"></i>
                            </a>
                            @endif

                            @if(can('view-'.$likesModule))
                            <a class="btn-xs" href="{{$likesModule}}?course_id={{$row->id}}" title="{{trans('courses.Likes')}}"  target="_blank">{{$row->likes->count()}} <i class="icon fa fa-thumbs-up"></i>
                            </a>
                            @endif
                            <img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}">
                            
                        </p>
                        <h5 class="blog-title">
                            {{$row->title}}
                        </h5>
                        <p class="blog-text">
                            {!! $row->content !!}
                        </p>
                        <p class="blog-text">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
                            <tr>
                                <td width="40%" class="align-left bold">{{trans('courses.Section')}}</td>
                                <td width="75%" class="align-left">{{$row->section->title}}</td>
                            </tr>
                            <tr>
                                <td width="40%" class="align-left bold">{{trans('courses.Level')}}</td>
                                <td width="75%" class="align-left">{{$row->level->title}}</td>
                            </tr>
                            <tr>
                                <td width="40%" class="align-left bold">{{trans('courses.Language')}}</td>
                                <td width="75%" class="align-left">{{$row->language->title}}</td>
                            </tr>
                            @foreach(langs() as $lang)
                            <tr>
                                <td width="40%" class="align-left bold">{{trans('courses.Tags')}} {{$lang}}</td>
                                <td width="75%" class="align-left">{{@$row->getTranslation('tags',$lang)}}</td>
                            </tr>
                            @endforeach

                            @foreach(langs() as $lang)
                            <tr>
                                <td width="40%" class="align-left bold">{{trans('courses.Meta description')}} {{$lang}}</td>
                                <td width="75%" class="align-left">{{@$row->getTranslation('meta_description',$lang)}}</td>
                            </tr>
                            @endforeach

                            @foreach(langs() as $lang)
                            <tr>
                                <td width="40%" class="align-left">{{trans('courses.Meta keywords')}} {{$lang}}</td>
                                <td width="75%" class="align-left">{{@$row->getTranslation('meta_keywords',$lang)}}</td>
                            </tr>
                            @endforeach
                        </table>
                        </p>
                    </div><!-- card-body -->
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script>
    $(function () {
        var group = $(".sortable").sortable({
            group: 'serialization',
            onDrop: function ($item, container, _super) {
                var data = group.sortable("serialize").get();
                console.log(data);
                        $.post("{{lang()}}/ajax/sort", {items:data[0], course_id:{{$row->id}}});
        _super($item, container);
        }
    });
    });
</script>
@endpush

@push('css')
<style>
    /* line 1, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    body.dragging, body.dragging * {
        cursor: move !important; }

    /* line 4, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    .dragged {
        position: absolute;
        top: 0;
        opacity: 0.5;
        z-index: 2000; }

    /* line 10, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical {
        margin: 0 0 9px 0;
        min-height: 10px; }
    /* line 13, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li {
        display: block;
        margin: 5px;
        padding: 5px;
        border: 1px solid #cccccc;
        background: #ffffff; }
    /* line 20, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li.placeholder {
        position: relative;
        margin: 0;
        padding: 0;
        border: none; }
    /* line 25, /Users/jonasvonandrian/jquery-sortable/source/css/jquery-sortable.css.sass */
    ol.vertical li.placeholder:before {
        position: absolute;
        content: "";
        width: 0;
        height: 0;
        margin-top: -5px;
        left: -5px;
        top: -4px;
        border: 5px solid transparent;
        border-left-color: red;
        border-right: none; }
</style>
@endpush