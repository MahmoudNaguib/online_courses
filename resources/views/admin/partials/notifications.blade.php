@if(auth()->user())
@php
// $notifications = auth()->user()->notifications()->unreaded()->latest()->take(5)->get();
@endphp
@if(@$notifications)
<!-- dropdown -->
<div class="dropdown dropdown-b">
    <a href="" class="header-notification" data-toggle="dropdown">
        <i class="icon ion-ios-bell-outline"></i>
        @if(!@$notifications->isEmpty())
        <span class="indicator">{{$notifications->count()}}</span>
        @endif
    </a>
    <div class="dropdown-menu">
        <div class="dropdown-menu-header">
            <h6 class="dropdown-menu-title">{{trans('app.Notifications')}}</h6>
            <div>
                <a href="notifications/read-all">{{trans('app.Mark all as Read')}}</a>
            </div>
        </div>
        <!-- dropdown-menu-header -->
        <div class="dropdown-list">
            @foreach(@$notifications as $notification)
            <!-- loop starts here -->
            <a href="notifications/to/{{$notification->id}}" class="dropdown-link">
                <div class="media">
                    <div class="media-body">
                        <p>{{$notification->message}}</p>
                        <span>{{$notification->created_at}}</span>
                    </div>
                </div>
                <!-- media -->
            </a>
            <!-- loop ends here -->
            @endforeach
            <div class="dropdown-list-footer">
                <a href="notifications"><i class="fa fa-angle-down"></i> {{trans('app.Show all')}}</a> |
                <a href="notifications/delete-all" data-confirm="{{trans('notifications.Are you sure you want to delete all notifications')}}?"><i class="fa fa-angle-down"></i> {{trans('app.Delete all notifications')}}</a>
            </div>
        </div>
        <!-- dropdown-list -->
    </div>
    <!-- dropdown-menu-right -->
</div>
<!-- dropdown -->
@endif
@endif