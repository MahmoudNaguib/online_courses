<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{(request()->is(lang()))?"active":""}}">
                <a class="nav-link" href="{{app()->make("url")->to('/')}}/{{lang()}}/admin">
                    <i class="icon ion-ios-pie-outline"></i>
                    <span>{{trans('navigation.Dashboard')}}</span>
                </a>
            </li>

            @if(can('create-users') || can('view-users'))
            <li class="nav-item {{(request()->is('*/users*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/users">
                    <i class="icon fa ion-ios-contact-outline"></i>
                    <span>{{trans('navigation.Users')}}</span>
                </a>
            </li>
            @endif
             
            @if(can('create-courses') || can('view-courses'))
            <li class="nav-item {{(request()->is('*/courses*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/courses">
                    <i class="icon fa ion-ios-compose"></i> 
                    <span>{{trans('navigation.Courses')}}</span>
                </a>
            </li>
            @endif

            @if(can('create-pages') || can('view-pages'))
            <li class="nav-item {{(request()->is('*/pages*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/pages">
                    <i class="icon fa ion-ios-compose"></i> 
                    <span>{{trans('navigation.Pages')}}</span>
                </a>
            </li>
            @endif

            @if(can('create-menu') || can('view-menu'))
            <li class="nav-item {{(request()->is('*/menu*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/menu">
                    <i class="icon fa ion-ios-compose"></i> 
                    <span>{{trans('navigation.Menu')}}</span>
                </a>
            </li>
            @endif

            @if(can('create-categories') || can('view-categories') || can('create-posts') || can('view-posts'))
            <li class="nav-item with-sub settings {{(request()->is('*/categories*') || request()->is('*/posts*'))?"active":""}}">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon fa ion-ios-compose"></i>
                    <span>{{trans('navigation.Blog')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        @if(can('create-categories') || can('view-categories'))
                        <li class="{{(request()->is('*/categories*'))?"active":""}}">
                            <a href="{{lang()}}/admin/categories">{{trans('navigation.Categories')}}</a>
                        </li>
                        @endif

                        @if(can('create-posts') || can('view-posts'))
                        <li class="{{(request()->is('*/posts*'))?"active":""}}">
                            <a href="{{lang()}}/admin/posts">{{trans('navigation.Posts')}}</a>
                        </li>
                        @endif
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif

            @if(can('view-comments') || can('view-likes') || can('view-rates') || can('create-sections') || can('view-sections'))
            <li class="nav-item with-sub settings {{(request()->is('*/rates*') || request()->is('*/likes*'))?"active":""}}">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon fa ion-ios-compose"></i>
                    <span>{{trans('navigation.Others')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        @if(can('create-sections') || can('view-sections'))
                        <li class="{{(request()->is('*/sections*'))?"active":""}}">
                            <a href="{{lang()}}/admin/sections">{{trans('navigation.Sections')}}</a>
                        </li>
                        @endif
                        @if(can('view-sections'))
                        <li class="{{(request()->is('*/comments*'))?"active":""}}">
                            <a href="{{lang()}}/admin/comments">{{trans('navigation.Comments')}}</a>
                        </li>
                        @endif
                        @if(can('view-likes'))
                        <li class="{{(request()->is('*/likes*'))?"active":""}}">
                            <a href="{{lang()}}/admin/likes">{{trans('navigation.Likes')}}</a>
                        </li>
                        @endif
                        @if(can('view-rates'))
                        <li class="{{(request()->is('*/rates*'))?"active":""}}">
                            <a href="{{lang()}}/admin/rates">{{trans('navigation.Rates')}}</a>
                        </li>
                        @endif
                        @if(can('view-contacts'))
                        <li class="{{(request()->is('*/contacts*'))?"active":""}}">
                            <a href="{{lang()}}/admin/contacts">{{trans('navigation.Newsletter Contacts')}}</a>
                        </li>
                        @endif
                        @if(can('view-messages'))
                        <li class="{{(request()->is('*/messages*'))?"active":""}}">
                            <a href="{{lang()}}/admin/messages">{{trans('navigation.Contact Messages')}}</a>
                        </li>
                        @endif
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif

            @if(@auth()->user()->is_super_admin)
            <li class="nav-item with-sub settings">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon ion-ios-gear-outline"></i>
                    <span>{{trans('navigation.Settings')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li class="{{(request()->is('*/configs*'))?"active":""}}">
                            <a href="{{lang()}}/admin/configs">{{trans('navigation.Configurations')}}</a>
                        </li>
                        <li class="{{(request()->is('*/translator*'))?"active":""}}">
                            <a href="{{lang()}}/admin/translator/edit/app">{{trans('navigation.Localization')}}</a>
                        </li>
                        <li class="{{(request()->is('*/roles*'))?"active":""}}">
                            <a href="{{lang()}}/admin/roles">{{trans('navigation.Roles')}}</a>
                        </li>
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif

        </ul>
    </div>
    <!-- container -->
</div>