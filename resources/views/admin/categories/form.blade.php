@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('categories.Title').' '.$lang,'placeholder'=>trans('categories.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('categories.Is active')]])



@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
        });
});
</script>
@endpush
