
@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('posts.Title').' '.$lang,'placeholder'=>trans('posts.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control editor','label'=>trans('posts.Content').' '.$lang,'placeholder'=>trans('posts.Content')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'content['.$lang.']','value'=>$row->getTranslation('content',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('posts.Meta description').' '.$lang,'placeholder'=>trans('posts.Meta description')];
@endphp

@include('form.input',['name'=>'meta_description['.$lang.']','value'=>$row->getTranslation('meta_description',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control tags','label'=>trans('posts.Meta keywords').' '.$lang,'placeholder'=>trans('posts.Meta keywords')];
@endphp

@include('form.input',['name'=>'meta_keywords['.$lang.']','value'=>$row->getTranslation('meta_keywords',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@php
$attributes=['class'=>'form-control','label'=>trans('posts.Title'),'placeholder'=>trans('posts.Title'),'required'=>1];
@endphp

@include('form.input',['name'=>'facebook_pixel','type'=>'textarea','attributes'=>['class'=>'form-control','label'=>trans('posts.Facebook pixel'),'placeholder'=>trans('posts.Facebook pixel')]])

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('posts.Image'),'placeholder'=>trans('posts.Image')]])

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('posts.Is active')]])

@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
            
            var content_en=$('textarea[name="content[en]"]').val();
            @foreach(langs() as $lang)
                if($('textarea[name="content[{{$lang}}]"]').val()=='')
                    $('textarea[name="content[{{$lang}}]"]').val(content_en);
            @endforeach
            
            var tags_en=$('input[name="tags[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="tags[{{$lang}}]"]').val()=='')
                    $('input[name="tags[{{$lang}}]"]').val(tags_en);
            @endforeach
            
            var meta_description_en=$('textarea[name="meta_description[en]"]').val();
            @foreach(langs() as $lang)
                if($('textarea[name="meta_description[{{$lang}}]"]').val()=='')
                    $('textarea[name="meta_description[{{$lang}}]"]').val(meta_description_en);
            @endforeach
            
            var meta_keywords_en=$('input[name="meta_keywords[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="meta_keywords[{{$lang}}]"]').val()=='')
                    $('input[name="meta_keywords[{{$lang}}]"]').val(meta_keywords_en);
            @endforeach

            
        });
});
</script>
@endpush
