<div class="mg-b-10">
    {!! Form::model($row,['method' => 'get','files' => true] ) !!} 
    <div class="row">
        
        <div class="col-lg-3 col-md-6 mg-t-10">
            {!! Form::select('category_id',  $row->getCategories(),@request('category_id'), ['class'=>'form-control select2','placeholder'=>trans('posts.Category')]) !!}
        </div><!-- col-4 -->
        
        <div class="col-lg-3 col-md-6 mg-t-10">
            <button class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Filter') }}</button> 
            <a href="{{$module}}" class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Reset') }}</a>
        </div>
    </div><!-- row -->
    {!! Form::close() !!}
</div><!-- section-wrapper -->