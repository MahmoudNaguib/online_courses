@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('create-'.$module))
    <a href="{{$module}}/create" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('app.Create')}}
    </a>
    @endif
    @if(can('view-'.$module))
    <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
    </a>
    @endif
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('view-'.$module))
    @include($module.'.partials.filters')
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('posts.ID')}} </th>
                    <th class="wd-15p">{{trans('posts.Category')}} </th>
                    <th class="wd-20p">{{trans('posts.Title')}} </th>
                    <th class="wd-5p"><i class="icon fa fa-comment"></i></th>
                    <th class="wd-5p"><i class="icon fa fa-thumbs-up"></i></th>
                    <th class="wd-5p">{{trans('posts.Views count')}} </th>
                    <th class="wd-5p">{{trans('posts.Active')}} </th>
                    <th class="wd-10p">{{trans('posts.Created by')}} </th>
                    <th class="wd-15p">{{trans('posts.Created at')}}</th>
                    <th class="wd-20p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->category->title}}</td>
                    <td class="center">{{str_limit($row->title,50)}}</td>
                    <td class="center">{{$row->comments->count()}}</td>
                    <td class="center">{{$row->likes->count()}}</td>
                    <td class="center">{{$row->views}}</td>
                    <td class="center"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
                    <td class="center">{{@$row->creator->name}}</td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">
                        @if(can('view-'.$commentsModule))
                        <a class="btn-xs" href="{{$commentsModule}}?post_id={{$row->id}}" title="{{trans('posts.Comments')}}" target="_blank">
                            <i class="icon fa fa-comment"></i>
                        </a>
                        @endif
                        
                        @if(can('view-'.$likesModule))
                        <a class="btn-xs" href="{{$likesModule}}?post_id={{$row->id}}" title="{{trans('posts.Likes')}}"  target="_blank">
                            <i class="icon fa fa-thumbs-up"></i>
                        </a>
                        @endif
                        
                        @if(can('edit-'.$module))
                        <a class="btn btn-success btn-xs" href="{{$module}}/edit/{{$row->id}}" title="{{trans('posts.Edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endif

                        @if(can('view-'.$module))
                        <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('posts.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        @endif

                        @if(can('delete-'.$module))
                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('posts.Delete')}}" data-confirm="{{trans('posts.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->appends(['created_by'=>request('created_by'),'category_id'=>request('category_id')])->render() !!}
    </div>
    @else
    {{trans("posts.There is no results")}}
    @endif
    @endif
</div>
@endsection
