@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('edit-'.$module))
    <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('posts.Edit')}}
    </a><br>
    @endif
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Total Comments')}} </td>
                <td width="75%" class="align-left">
                    @if(can('view-'.$commentsModule))
                    <a class="btn-xs" href="{{$commentsModule}}?post_id={{$row->id}}" title="{{trans('posts.Comments')}}" target="_blank">
                        {{$row->comments->count()}}  <i class="icon fa fa-comment"></i>
                    </a>
                    @else
                    {{$row->comments->count()}}  <i class="icon fa fa-comment"></i>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Total likes')}}</td>
                <td width="75%" class="align-left">
                    
                    @if(can('view-'.$likesModule))
                    <a class="btn-xs" href="{{$likesModule}}?post_id={{$row->id}}" title="{{trans('posts.Likes')}}"  target="_blank">
                        {{$row->likes->count()}} <i class="icon fa fa-thumbs-up"></i>
                    </a>
                    @else
                        {{$row->likes->count()}} <i class="icon fa fa-thumbs-up"></i>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Category')}}</td>
                <td width="75%" class="align-left">{{$row->category->title}}</td>
            </tr>
            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Title')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('title',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Slug')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('slug',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Content')}} {{$lang}}</td>
                <td width="75%" class="align-left">{!! @$row->getTranslation('content',$lang) !!}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Tags')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('tags',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Meta description')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('meta_description',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Meta keywords')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('meta_keywords',$lang)}}</td>
            </tr>
            @endforeach

            <tr>
                <td width="25%" class="align-left">{{trans('posts.Image')}}</td>
                <td width="75%" class="align-left">{!! image($row->image,'small') !!}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Is active')}}</td>
                <td width="75%" class="align-left"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('posts.Created by')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>

        </table>
    </div>
</div>
@endsection
