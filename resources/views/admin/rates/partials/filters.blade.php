<div class="mg-b-10">
    {!! Form::model($row,['method' => 'get','files' => true] ) !!} 
    <div class="row">
        @if(request('user_id'))
        <div class="col-lg-4 col-md-6 mg-t-10">
            {!! Form::select('user_id',  $row->getTeacher(),@request('user_id'), ['class'=>'form-control select2','placeholder'=>trans('rates.Teachers'),'id'=>'user_id']) !!}
        </div><!-- col-4 -->
        @endif
        
        @if(request('course_id'))
        <div class="col-lg-4 col-md-6 mg-t-10">
            {!! Form::select('course_id',  $row->getCourses(),@request('course_id'), ['class'=>'form-control select2','placeholder'=>trans('rates.Course'),'id'=>'course_id']) !!}
        </div><!-- col-4 -->
        @endif
        
        <div class="col-lg-3 col-md-6 mg-t-10">
            <button class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Filter') }}</button> 
            <a href="{{$module}}" class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Reset') }}</a>
        </div>
    </div><!-- row -->
    {!! Form::close() !!}
</div><!-- section-wrapper -->

@push('js')
<script>
    $(function(){
        $('#user_id').on('change',function(){
           if($(this).val()!='' && $(this).val()!=undefined){
               $('#course_id').val('');
           } 
        });
        $('#course_id').on('change',function(){
           if($(this).val()!='' && $(this).val()!=undefined){
               $('#user_id').val('');
           } 
        });
    });
</script>
@endpush