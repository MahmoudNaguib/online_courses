<div class="mg-b-10">
    {!! Form::model($row,['method' => 'get','files' => true] ) !!} 
    <div class="row">
        <div class="col-lg-4 col-md-6 mg-t-10">
            {!! Form::select('post_id',  $row->getPosts(),@request('post_id'), ['class'=>'form-control select2','placeholder'=>trans('comments.Post'),'id'=>'post_id']) !!}
        </div><!-- col-4 -->
        <div class="col-lg-4 col-md-6 mg-t-10">
            {!! Form::select('course_id',  $row->getCourses(),@request('course_id'), ['class'=>'form-control select2','placeholder'=>trans('comments.Course'),'id'=>'course_id']) !!}
        </div><!-- col-4 -->
        <div class="col-lg-3 col-md-6 mg-t-10">
            <button class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Filter') }}</button> 
            <a href="{{$module}}" class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Reset') }}</a>
        </div>
    </div><!-- row -->
    {!! Form::close() !!}
</div><!-- section-wrapper -->

@push('js')
<script>
    $(function(){
        $('#post_id').on('change',function(){
           if($(this).val()!='' && $(this).val()!=undefined){
               $('#course_id').val('');
           } 
        });
        $('#course_id').on('change',function(){
           if($(this).val()!='' && $(this).val()!=undefined){
               $('#post_id').val('');
           } 
        });
    });
</script>
@endpush