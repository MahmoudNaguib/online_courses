@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('view-'.$module))
    <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
    </a>
    @endif
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('view-'.$module))
    @include($module.'.partials.filters')
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('comments.ID')}} </th>
                    <th class="wd-5p">{{trans('comments.Type')}} </th>
                    <th class="wd-15p">{{trans('comments.Title')}}</th>
                    <th class="wd-20p">{{trans('comments.Content')}} </th>
                    <th class="wd-10p">{{trans('comments.Created by')}} </th>
                    <th class="wd-15p">{{trans('comments.Created at')}}</th>
                    <th class="wd-15p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center">{{$row->type}}</td>
                    <td class="center">
                         {{$row->commentable->title}}
                    </td>
                    <td class="center">{{$row->content}}</td>
                    <td class="center">{{@$row->creator->name}}</td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">
                        @if(can('delete-'.$module))
                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('comments.Delete')}}" data-confirm="{{trans('comments.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->appends(['post_id'=>request('post_id'),'course_id'=>request('course_id')])->render() !!}
    </div>
    @else
    {{trans("comments.There is no results")}}
    @endif
    @endif
</div>
@endsection
