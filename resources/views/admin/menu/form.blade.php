@include('form.select',['name'=>'parent_id','options'=>$row->getMainMenuItems(),'attributes'=>['class'=>'form-control','label'=>trans('menu.Parent'),'placeholder'=>trans('menu.Main item')]])

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('menu.Title').' '.$lang,'placeholder'=>trans('menu.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@include('form.input',['name'=>'order_field','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('menu.Order'),'placeholder'=>trans('menu.order'),'step'=>'1','required'=>1,'min'=>0,'pattern'=>'^\d*(\.\d{0,2})?$']])

@include('form.select',['name'=>'page_id','options'=>$row->getPages(),'attributes'=>['class'=>'form-control','label'=>trans('menu.Page'),'placeholder'=>trans('menu.Select page')]])


@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('menu.Is active')]])

@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
           
        });
});
</script>
@endpush
