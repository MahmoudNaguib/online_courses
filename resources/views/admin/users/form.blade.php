@include('form.select',['name'=>'role_id','options'=>$row->getRoles(),'attributes'=>['id'=>'role_id','class'=>'form-control','label'=>trans('users.Role'),'placeholder'=>trans('users.Role')]])

@include('form.input',['name'=>'name','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Name'),'placeholder'=>trans('users.Name'),'required'=>1]])

@include('form.input',['name'=>'email','type'=>'email','attributes'=>['class'=>'form-control','label'=>trans('users.Email'),'placeholder'=>trans('users.Email'),'required'=>1]])

@include('form.input',['name'=>'mobile','type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('users.Mobile'),'placeholder'=>trans('users.Mobile'),'required'=>1]])

@include('form.select',['name'=>'language','options'=>langs(),'attributes'=>['class'=>'form-control','label'=>trans('users.Default language'),'placeholder'=>trans('users.Default language'),'required'=>1]])

@php 
$attributes=['class'=>'form-control','label'=>trans('users.Password'),'placeholder'=>trans('users.Password'),'required'=>1];
if(@$row->id) unset($attributes['required']);
@endphp
@include('form.password',['name'=>'password','attributes'=>$attributes])

@php 
$attributes=['class'=>'form-control','label'=>trans('users.Password confirmation'),'placeholder'=>trans('users.Password confirmation'),'required'=>1];
if(@$row->id) unset($attributes['required']);
@endphp


@include('form.password',['name'=>'password_confirmation','attributes'=>$attributes])


@include('form.boolean',['name'=>'is_teacher','attributes'=>['label'=>trans('users.Is teacher')]])

@foreach(langs() as $lang)
@php
$attributes=['class'=>'form-control editor','label'=>trans('users.Bio').' '.$lang,'placeholder'=>trans('users.Bio')];
@endphp
@include('form.input',['name'=>'bio['.$lang.']','value'=>$row->getTranslation('bio',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach


@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('users.Image'),'placeholder'=>trans('users.Image')]])


@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var bio_en=$('input[name="bio[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="bio[{{$lang}}]"]').val()=='')
                    $('input[name="bio[{{$lang}}]"]').val(title_en);
            @endforeach
            
        });
});
</script>
@endpush

