@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
    @if(can('create-'.$module))
    <a href="{{$module}}/create" class="btn btn-success">
        <i class="fa fa-plus"></i> {{trans('app.Create')}}
    </a>
    @endif
    @if(can('view-'.$module))
    <a href="{{$module}}/export?{{@$_SERVER['QUERY_STRING']}}" class="btn btn-primary">
        <i class="fa fa-arrow-down"></i> {{trans('app.Export')}}
    </a>
    @endif

</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('view-'.$module))
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-5p">{{trans('users.ID')}} </th>
                    <th class="wd-5p">{{trans('users.Is Admin')}} </th>
                    <th class="wd-5p">{{trans('users.Is Teacher')}} </th>
                    <th class="wd-15p">{{trans('users.Name')}} </th>
                    <th class="wd-15p">{{trans('users.Email')}} </th>
                    <th class="wd-15p">{{trans('users.Mobile')}} </th>
                    <th class="wd-15p">{{trans('users.Created at')}}</th>
                    <th class="wd-15p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center">{{$row->id}}</td>
                    <td class="center"><img src="backend/img/{{($row->is_admin)?'check.png':'close.png'}}"></td>
                    <td class="center"><img src="backend/img/{{($row->is_teacher)?'check.png':'close.png'}}"></td>
                    <td class="center">{{$row->name}}</td>
                    <td class="center">{{$row->email}}</td>
                    <td class="center">{{$row->mobile}}</td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">
                        <a class="btn btn-success btn-xs" href="{{$module}}/edit/{{$row->id}}" title="{{trans('users.Edit')}}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('users.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('users.Delete')}}" data-confirm="{{trans('users.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="paganition-center"> 
        {!! $rows->render() !!}
    </div>
    @else
    {{trans("users.There is no results")}}
    @endif
    @endif
</div>
@endsection
