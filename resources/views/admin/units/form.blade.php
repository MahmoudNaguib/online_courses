@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('units.Title').' '.$lang,'placeholder'=>trans('units.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('units.Is active')]])


@php
    $course_id=(request('course_id'))?:$row->course_id;
@endphp
<input type="hidden" name="course_id" value="{{$course_id}}">
@php
    $lastUnit=\App\Models\Unit::where('course_id',$course_id)->orderBy('order_field','desc')->first();
@endphp
<input type="hidden" name="order_field" value="{{($lastUnit)?($lastUnit->order_field+1):1}}">

@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
        });
});
</script>
@endpush
