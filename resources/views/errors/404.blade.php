@extends('front.layouts.master')

@section('content')
<div class="latest-area section-padding bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.404')}}</h3>
                        <p>{{trans('app.Page not found')}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <p>{{trans('app.You may have mistyped the address or the page may have moved')}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection