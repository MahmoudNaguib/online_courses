@extends('layouts.auth')
@section('content')
<div class="signin-box">
    <h3 class="signin-title-secondary">{{$page_title}}</h3>
    {!! Form::model($row,['method' => 'post','files' => true] ) !!} 
    {{ csrf_field() }}
    @php 
    $attributes=['class'=>'form-control','label'=>trans('profile.Name'),'placeholder'=>trans('profile.Name'),'required'=>1];
    @endphp
    @include('form.input',['name'=>'name','type'=>'text','attributes'=>$attributes])

    @php 
    $attributes=['class'=>'form-control','label'=>trans('profile.Email'),'placeholder'=>trans('profile.Email'),'required'=>1];
    @endphp
    @include('form.input',['name'=>'email','type'=>'email','attributes'=>$attributes])

    @php 
    $attributes=['class'=>'form-control','label'=>trans('profile.Mobile'),'placeholder'=>trans('profile.Mobile'),'required'=>1];
    @endphp
    @include('form.input',['name'=>'mobile','type'=>'text','attributes'=>$attributes])

    @php 
    $options=languages(); 
    $attributes=['class'=>'form-control','label'=>trans('profile.Default language'),'placeholder'=>trans('profile.Default language'),'required'=>1];
    @endphp
    @include('form.select',['name'=>'language','options'=>$options,'attributes'=>$attributes])

    @foreach(langs() as $lang)
    @php
    $attributes=['class'=>'form-control','label'=>trans('profile.Bio').' '.$lang,'placeholder'=>trans('profile.Bio')];
    @endphp

    @include('form.input',['name'=>'bio['.$lang.']','value'=>$row->getTranslation('bio',$lang),'type'=>'textarea','attributes'=>$attributes])
    @endforeach


    @foreach(langs() as $lang)
    @php
    $attributes=['class'=>'form-control','label'=>trans('profile.Meta description').' '.$lang,'placeholder'=>trans('profile.Meta description')];
    @endphp

    @include('form.input',['name'=>'meta_description['.$lang.']','value'=>$row->getTranslation('meta_description',$lang),'type'=>'textarea','attributes'=>$attributes])
    @endforeach

    @foreach(langs() as $lang)
    @php
    $attributes=['class'=>'form-control tags','label'=>trans('profile.Meta keywords').' '.$lang,'placeholder'=>trans('profile.Meta keywords')];
    @endphp

    @include('form.input',['name'=>'meta_keywords['.$lang.']','value'=>$row->getTranslation('meta_keywords',$lang),'type'=>'text','attributes'=>$attributes])
    @endforeach



    @include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('profile.Image'),'placeholder'=>trans('profile.Image')]])


    <!-- custom-file -->
    <div class="form-layout-footer mg-t-30">
        <button class="btn btn-primary bd-0">{{ trans('profile.Save') }}</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection
