@extends('layouts.auth')
@section('content')
<div class="signin-box">
    <h3 class="signin-title-secondary">{{$page_title}}</h3>
    {!! Form::model($row,['method' => 'post','files' => true] ) !!} 
    {{ csrf_field() }}
    @php 
    $attributes=['class'=>'form-control','label'=>trans('profile.Password'),'placeholder'=>trans('profile.Password')];
    @endphp
    @include('form.password',['name'=>'password','attributes'=>$attributes])

    @php 
    $attributes=['class'=>'form-control','label'=>trans('profile.Password confirmation'),'placeholder'=>trans('profile.Password confirmation')];
    @endphp
    @include('form.password',['name'=>'password_confirmation','attributes'=>$attributes])
    <!-- custom-file -->
    <div class="form-layout-footer mg-t-30">
        <button class="btn btn-primary bd-0">{{ trans('profile.Save') }}</button>
    </div>
    {!! Form::close() !!}
</div>
@endsection
