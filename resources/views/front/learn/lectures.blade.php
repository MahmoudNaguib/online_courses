@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title">
                <h3>{{$page_title}}</h3>
                <p></p>
            </div>
        </div>
    </div>
</div>
<div class="course-details-area section-padding mb-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-9 col-12">
                @if($row->units)
                <div class="row">
                    <div class="accordion" id="accordionExample">
                        @foreach ($row->units as $unit)
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#unit_{{$unit->id}}" aria-expanded="true" aria-controls="collapseOne">
                                        {{$unit->title}} ({{$unit->lectures()->count()}} {{trans('app.Lectures')}})
                                    </button>
                                </h2>
                            </div>

                            <div id="unit_{{$unit->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    @if($unit->lectures)
                                    <ol>
                                        @foreach($unit->lectures as $lecture)
                                        <li data-id='{{$lecture->id}}' data-name='lectures' class="{{($lecture->id==$lecture_id)?'current_lecture':''}}">
                                            <a href="{{lang()}}/learn?course_id={{$row->id}}&lecture_id={{$lecture->id}}">{{$lecture->title}}</a>
                                        </li>
                                        @endforeach
                                    </ol> 
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-9 col-md-9 col-12">
                <div class="section-title mb-40"><h4>{{$lec->title}}</h4></div>
                <div class="single-item-text">
                    <h4><a href="{{$row->link}}">{{$row->title}}</a></h4>
                    <div class="single-item-text-info">
                        <span>{{trans('app.By')}}: <span>{{$row->creator->name}}</span></span>
                        <span>{{trans('app.Date')}}: <span>{{$row->created_at->format('F j, Y')}}</span></span>
                        <span>{{trans('app.Attachment')}}:  <span><a href="uploads/{{$lec->attachment}}"><i class="fa fa-download fa-1x"></i> {{trans('app.Download')}}</a></span></span>
                        @if($lec->attachment)
                        @endif
                    </div>
                </div>
                @if($lec->video)
                <div class="videoPlayer">
                    <div class="embed-responsive embed-responsive-16by9"> 
                        <video width="320" height="240" controls>
                            <source src="uploads/{{$lec->video}}" type="video/mp4">
                        </video>
                    </div> 
                </div>
                @endif
                <p class="mt-30">{!! $lec->content !!}</p>
            </div>
        </div>
    </div>
</div>
@endsection



@push('js')
<script>
    $(function () {
        console.log($('.current_lecture').parent().parent().parent().attr('class'));
        $('.current_lecture').parent().parent().parent().addClass('show');
    });
</script>
@endpush