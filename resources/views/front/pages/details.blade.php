@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title">
                <h3>{{$page_title}}</h3>
                <p></p>
            </div>
        </div>
    </div>
</div>
<!--News Details Area Start-->
<div class="news-details-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="news-details-content">
                    <div class="single-latest-item">
                        @if($row->image)
                        <div class="col-md-6 offset-md-3 text-center">
                            <img src="uploads/large/{{$row->image}}" alt="{{$row->title}}" class="img-fluid">
                        </div>
                        @endif
                        <div class="single-latest-text">
                            <div class="single-item-comment-view">
                                <span><i class="zmdi zmdi-calendar-check"></i>{{$row->created_at->format('F j, Y')}}</span>
                            </div>
                            <p>{!! $row->content !!}</p>
                            <div class="tags-and-links">
                                <div class="social-links">
                                    <span>{{trans('app.Share')}}:</span>
                                    <a href="https://facebook.com/sharer/sharer.php?u={{$row->link}}" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
                                    <a href="https://twitter.com/intent/tweet?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-twitter"></i></a>
                                    <a href="https://www.linkedin.com/shareArticle?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-linkedin"></i></a>
                                    <a href="https://web.whatsapp.com/send?text={{$row->link}}" target="_blank"><i class="zmdi zmdi-whatsapp"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
