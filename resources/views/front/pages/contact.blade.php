@extends('front.layouts.master')

@section('content')
<div class="breadcrumb-banner-area" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{$image}}) no-repeat scroll 0 0;">
</div>

<div class="about-page-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title mt-20">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="about-text-container mb-50">
                    <p>{!! $content!!}</p>
                    <form method="post" action="{{lang()}}/messages"  id="contact_us"class="auth-form mb-20">
                        <div class="form-group">
                            <label for="name">{{trans('app.Name')}}</label> <i class="fa fa-user"></i>
                            <input class="form-control" id="name" required="required" name="name" type="text" placeholder="{{trans('app.Name')}}" aria-required="true">
                            @if(@$errors)
                            @foreach($errors->get('name') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                            @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="name">{{trans('app.Mobile')}}</label> <i class="fa fa-phone"></i>
                            <input class="form-control" id="mobile" required="required" name="mobile" type="text" placeholder="{{trans('app.Mobile')}}" aria-required="true">
                            @if(@$errors)
                            @foreach($errors->get('name') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                            @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">{{trans('app.Email')}}</label> <i class="fa fa-envelope-o"></i>
                            <input class="form-control" id="email" required="required" name="email" type="email" placeholder="{{trans('app.Email')}}">
                            @if(@$errors)
                            @foreach($errors->get('name') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                            @endforeach
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{trans('app.Your message')}}</label> <i class="fa fa-commenting-o"></i>
                            <textarea class="form-control" required="required" id="content" name="content" cols="30" rows="10" placeholder="{{trans('app.Your message')}}"></textarea>
                            @if(@$errors)
                            @foreach($errors->get('name') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                            @endforeach
                            @endif
                        </div>
                        <div class="form-group mb-0">
                            <button class="btn btn-theme btn-success" type="submit">{{trans('app.Send Message')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
