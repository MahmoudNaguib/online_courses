@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title">
                <h3>{{$page_title}}</h3>
                <p></p>
            </div>
        </div>
    </div>
</div>
<div class="course-details-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="course-details-content">
                    <div class="single-course-details">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="overlay-effect">
                                    <a href="#">
                                        <img src="uploads/large/{{$row->image}}" alt="{{$row->title}}">
                                    </a>
                                </div>
                            </div>  
                            <div class="col-md-6">
                                <div class="single-item-text">
                                    <h4>{{$row->title}}</h4>
                                    <div class="single-item-text-info">
                                        <span>{{trans('app.By')}}: <span>{{$row->creator->name}}</span></span>
                                        <span>{{trans('app.Date')}}: <span>{{str_limit($row->created_at,10,false)}}</span></span>
                                    </div>
                                    <div class="course-text-content">
                                        <p>{!! $row->content !!}</p>
                                        <p class="course_price">
                                            @if(@$coupon && @$coupon->discount)
                                            <strike>{{$row->price}}</strike>
                                            {{$row->total}}
                                            <span class="currency">{{trans('app.EGP')}}</span>
                                            @else
                                            {{$row->price}} <span class="currency">{{trans('app.EGP')}}</span>  
                                            @endif
                                            <form method="post" action="{{lang()}}/orders/add-coupon/{{$row->id}}" class="form-inline">
                                                <div class="form-group">
                                                    <label class="sr-only">{{trans('app.Coupon')}}</label>
                                                    <input name="code" type="text" class="form-control" placeholder="{{trans('app.Coupon')}}" value="{{request('code')}}">
                                                    <button class="btn btn-success">{{trans('app.Use')}}</button>
                                                </div>

                                            </form>
                                        </p>
                                        <p>
                                        <form method="post">
                                            <button class="btn btn-success btn-lg btn-block">{{trans('app.Buy')}}</button>
                                        </form>
                                        </p>
                                    </div>    
                                    @include('front.courses.counts') 
                                </div>
                            </div> 
                        </div>     
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
