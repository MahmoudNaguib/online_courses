@extends('front.layouts.master')

@section('content')
<!--End of Header Area-->
<!--Background Area Start-->
<div class="background-area no-animation">
    <img src="uploads/large/{{conf('home_banner')}}" alt="Home Banner"/>
    <div class="banner-content static-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content-wrapper full-width">
                        <div class="text-content text-center">
                            <h1 class="title1 text-center mb-20">
                                <span class="tlt block" data-in-effect="rollIn" data-out-effect="fadeOutRight" >{{conf('home_banner_text')}}</span>

                            </h1>
                            <div class="banner-readmore wow bounceInUp" data-wow-duration="2500ms" data-wow-delay=".1s">
                                <a class="button-default" href="{{lang()}}/courses">{{trans('app.View Courses')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Background Area-->
<!--About Area Start-->
<div class="about-area" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{conf('why_banner')}}) no-repeat scroll center top;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="about-container">
                    <h3>{{trans('app.Why us')}}?</h3>
                    <p>
                        {{conf('home_why_text')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!--End of About Area-->
@if (!$latestCourses->isEmpty())
<!--Course Area Start-->
<div class="course-area section-padding mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Latest Courses')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($latestCourses as $row)
            @include('front.courses.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/courses" class="button-default button-large">{{trans('app.Browse All Courses')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
<!--End of Course Area-->
@endif
<!--Fun Factor Area Start-->
<div class="fun-factor-area mt-30" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{conf('facts_banner')}}) repeat scroll 0 0;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper white">
                    <div class="section-title">
                        <h3>{{trans('app.IMPORTANT FACTS')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
                <div class="single-fun-factor">
                    <h4>{{trans('app.Teachers')}}</h4>
                    <h2><span class="counter">{{\App\Models\User::active()->IsTeacher()->get()->count()}}</span>+</h2>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="single-fun-factor">
                    <h4>{{trans('app.Members')}}</h4>
                    <h2><span class="counter">{{\App\Models\User::active()->get()->count()}}</span>+</h2>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="single-fun-factor">
                    <h4>{{trans('app.Courses')}}</h4>
                    <h2><span class="counter">{{\App\Models\Course::active()->get()->count()}}</span>+</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Fun Factor Area-->
<!--Latest News Area Start-->
@if (!$latestPosts->isEmpty())
<div class="latest-area section-padding bg-white mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Latest News')}}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($latestPosts as $row)
            @include('front.posts.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/posts" class="button-default button-large">{{trans('app.Browse All Posts')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
@endif
<!--End of Latest News Area-->

@if($teachers)
<!--Teachers Area Start-->
<div class="teachers-area padding-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('app.Our teachers')}}</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($teachers as $row)
            <div class="col-lg-3 col-md-6 col-12">
                <div class="single-teacher-item">
                    <div class="single-teacher-image">
                        <a href="{{$row->link}}">
                            <img src="uploads/small/{{$row->image}}" alt="{{$row->name}}">
                        </a>
                    </div>
                    <div class="single-teacher-text">
                        <h3>
                            <a href="{{$row->link}}">
                                {{$row->name}}
                            </a>
                        </h3>
                        <h4>{{trans('app.Teacher')}}</h4>
                        <p>{{$row->bio_limited}}</p>
                        @include('front.bio.partials.share')
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!--End of Teachers Area-->
@endif

@endsection
