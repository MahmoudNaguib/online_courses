<a href="{{lang()}}/contact" class="white">{{trans('app.Contact us')}}</a>
<div class="content">
    @if(auth()->user())
    <i class="zmdi zmdi-account"></i> {{trans('app.Welecome')}} {{str_limit(auth()->user()->name,10)}}
    <ul class="account-dropdown">
        @if(auth()->user()->is_admin)
        <li><a href="{{lang()}}/admin">{{ trans('app.Admin Dashboard') }}</a></li>
        @endif
        @if(auth()->user()->is_teacher)
        <li><a href="{{lang()}}/teacher">{{ trans('app.Teacher Dashboard') }}</a></li>
        @endif
        <li><a href="{{lang()}}/courses/my-courses">{{trans('app.My Courses')}}</a></li>
        <li><a href="{{lang()}}/favourites">{{trans('app.Favourite Courses')}}</a></li>
        <li><a href="{{lang()}}/profile/edit">{{trans('app.Edit account')}}</a></li>
        <li><a href="{{lang()}}/profile/change-password">{{trans('app.Change password')}}</a></li>
        <li><a href="{{lang()}}/profile/logout">{{trans('app.Logout')}}</a></li>

    </ul>
    @else
    <a href="{{lang()}}/auth/login">{{trans('app.Login')}}</a> |
    <a href="{{lang()}}/auth/register">{{trans('app.Register')}}</a>
    @endif
</div>