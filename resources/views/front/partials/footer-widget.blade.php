<div class="footer-widget-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-footer-widget">
                    <h3>{{trans('app.GET IN TOUCH')}}</h3>
                    <a href="tel:{{conf('phone')}}" target="_blank"><i class="fa fa-phone"></i>{{conf('phone')}}</a>
                    <a href="tel:{{conf('mobile')}}" target="_blank"><i class="fa fa-mobile"></i>{{conf('mobile')}}</a>
                    <a href="mailto:{{conf('email')}}" target="_blank"><i class="fa fa-envelope"></i>{{conf('email')}}</a>
                     <a href="{{conf('map_url')}}" target="_blank"><i class="fa fa-map-marker"></i>{{conf('address')}}</a>
                    <div class="social-icons">
                        @if(conf('facebook_link'))
                        <a href="{{conf('facebook_link')}}" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
                        @endif
                        @if(conf('twitter_link'))
                        <a href="{{conf('twitter_link')}}" target="_blank"><i class="zmdi zmdi-twitter"></i></a>
                        @endif
                        @if(conf('instagram_link'))
                        <a href="{{conf('instagram_link')}}" target="_blank"><i class="zmdi zmdi-instagram"></i></a>
                        @endif
                        @if(conf('youtube_link'))
                        <a href="{{conf('youtube_link')}}" target="_blank"><i class="zmdi zmdi-youtube"></i></a>
                        @endif
                    </div>
                </div>
            </div>
            @if($latestPosts)
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-footer-widget">
                    <h3>{{trans('app.Latest posts')}}</h3>
                    <ul class="footer-list">
                        @foreach($latestPosts as $row)
                        <li><a href="{{$row->link}}">{{$row->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
            @php 
                $pages=\App\Models\Page::active()->get();
            @endphp
            @if($pages)
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-footer-widget">
                    <h3>{{trans('app.Useful links')}}</h3>
                    <ul class="footer-list">
                        @foreach($pages as $row)
                        <li><a href="{{$row->link}}">{{$row->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>