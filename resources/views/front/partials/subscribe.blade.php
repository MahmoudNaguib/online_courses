@if(request()->url()==app()->make("url")->to('/').'/'.lang())
<div class="newsletter-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <div class="newsletter-content">
                    <h3>{{trans('app.NewsLetter')}}</h3>
                </div>
            </div>
            <div class="col-lg-7 col-md-7">
                <div class="newsletter-form angle">
                    <form method="post" action="{{lang()}}/contacts" class="mc-form footer-newsletter fix" id="subscribe-form">
                        <div class="subscribe-form">
                            <input id="mc-email" type="text" name="email" placeholder="{{trans('app.Email')}}">
                            <button id="mc-submit" type="submit">{{trans('app.Subscribe')}}</button>

                            <div class="alert alert-danger display-none" id="errors_subscribe_email"></div>
                            <div id="subscribe-message" class="alert alert-success display-none" role="alert"></div>
                        </div>    
                    </form>
                    <!-- mailchimp-alerts Start -->
                    <div class="mailchimp-alerts text-centre fix pull-right">
                        @if(@$errors)
                        @foreach($errors->get('email') as $message)
                        <div class="help-inline text-danger">{{ $message }}</div>
                        @endforeach
                        @endif
                    </div>
                    <!-- mailchimp-alerts end -->
                </div>
            </div>
        </div>
    </div>
</div>
@endif