<li class="{{(request()->url()==app()->make("url")->to('/').'/'.lang())?'active':''}}">
    <a href="{{lang()}}">{{trans('app.Home')}}</a>
</li>

<li class="{{request()->is('*/about')?'active':''}}"><a href="{{lang()}}/about">{{trans('app.About')}}</a></li>
<li class="{{request()->is('*/courses')?'active':''}}"><a href="{{lang()}}/courses">{{trans('app.Courses')}}</a></li>
<li class="{{request()->is('*/posts')?'active':''}}"><a href="{{lang()}}/posts">{{trans('app.Articles')}}</a></li>


@php
$items=\App\Models\Menu::where('parent_id',NULL)->active()->get();
@endphp

@if($items)
@foreach($items as $parent)
<li class="">
    <a href="{{($parent->page_id)?$parent->page->link:'#'}}">{{$parent->title}}</a>
    @if($childs=$parent->childs()->active()->get())
    <ul class="sub-menu">
        @foreach($childs as $child)
        <li><a href="{{$child->page->link}}">{{$child->title}}</a></li>
        @endforeach
    </ul>
    @endif
</li>
@endforeach
@endif