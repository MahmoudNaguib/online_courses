<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="pull-left">
                    <span>{{trans('app.Phone')}}: {{conf('phone')}}</span>&nbsp;&nbsp;&nbsp;
                    <span>{{trans('app.Email')}}: {{conf('email')}}</span>
                    <span>
                        @if(lang()=='ar')
                        <a href="{{urlLang(url()->full(),lang(),'en')}}" class="nav-link langSwitch">English</a>
                        @else
                        <a href="{{urlLang(url()->full(),lang(),'ar')}}" class="nav-link langSwitch">عربي</a>
                        @endif
                    </span>
                      
                    <!--End of Search Form-->
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="header-top-right pull-right">
                    <ul class="header-search">
                        <li class="search-menu">
                            <i id="toggle-search" class="zmdi zmdi-search-for"></i>
                        </li>
                    </ul>
                    <!--Search Form-->
                    <div class="search">
                        <div class="search-form">
                            <form id="search-form" action="{{lang()}}/courses">
                                <input type="search" placeholder="{{trans('app.Search here')}}..." name="q" />
                                <button type="submit">
                                    <span><i class="fa fa-search"></i></span>
                                </button>
                            </form>                                
                        </div>
                    </div>  
                    @include('front.partials.user_nav')
                    
                </div>
            </div>
        </div>
    </div>
</div>