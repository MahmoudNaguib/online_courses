<!-- Required meta tags -->
<meta charset="utf-8">
<base href="{{app()->make("url")->to('/')}}/" />
<title>{{ appName() }} :: {{@$page_title}}</title>
<meta name="description" content="{{@$meta_description}}">
<meta name="keywords" content="{{@$meta_keywords}}">
<meta name="author" content="{{appName()}}">
<meta name="language" content="{{(lang()=='ar')?'Arabic':'English'}}">

<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
<link rel="manifest" href="favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta http-equiv="Content-Language" content="en" />
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="theme-color" content="#4188c9">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">


<meta property="og:locale" content="{{(lang() == 'en')?'en_EG':'ar_EG'}}">
<meta property="og:type" content="website">
<link rel="canonical" href="{{App::make("url")->to('/')}}/{{Request::path()}}" />
<meta property="og:title" content="{{conf('application_name')}} - {{@$page_title}}"/>
<meta property="og:url" content="{{App::make("url")->to('/')}}/{{Request::path()}}"/>
<meta property="og:site_name" content="{{conf('application_name')}}"/>
<meta property="og:description" content="{{@$meta_description}}" />
<meta property="og:image:width" content="500" />
<meta property="og:image:height" content="500" />
<meta property="og:image" content="{{app()->make("url")->to('/')}}/uploads/large/{{@$image}}" />
<meta property="fb:app_id" content="966242223397117" />