@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title mt-25">
                <h3>{{$page_title}}</h3>
                <p></p>
            </div>
        </div>
    </div>
</div>
<div class="course-details-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="course-details-content">
                    <div class="single-course-details">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="overlay-effect">
                                    <a href="#">
                                        <img src="uploads/large/{{$row->image}}" alt="{{$row->name}}">
                                    </a>
                                </div>
                            </div>  
                            <div class="col-md-6">
                                <div class="single-item-text">
                                    <h4>{{$row->name}}</h4>
                                    <div class="course-text-content">
                                        <p>{!! $row->bio !!}</p>
                                        <div class="row news-details-content ">
                                            @include('front.bio.partials.share')
                                        </div>
                                    </div>   
                                </div>
                            </div> 

                        </div>     
                    </div>
                    <div class="comments contact-form-area">
                        @if(auth()->user())
                        <h4 class="title">{{trans('app.Write your comment')}}</h4>
                        <form id="contact-form" action="{{lang()}}/comments?commentable_id={{$row->id}}&type=user" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea name="content" cols="30" rows="10" placeholder="{{trans('app.Message')}}"></textarea>
                                    <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                </div>
                            </div>
                        </form>
                        @else
                        <h4 class="title">{{trans('app.Need to write your comment')}} <a href="{{lang()}}/auth/login">{{trans('app.Login')}}</a></h4>
                        @endif
                    </div>
                    @php
                    $comments=$row->comments()->with(['replies'])->where('reply_to',NULL)->latest()->get();
                    @endphp
                    @if($comments)
                    <div class="comments">
                        <h4 class="title">{{trans('app.Comments')}}</h4>
                        @foreach($comments as $comment)
                        <div class="single-comment">
                            <div class="author-image">
                                <img src="uploads/small/{{$comment->creator->image}}" alt="{{$comment->creator->name}}-{{$comment->id}}">
                            </div>
                            <div class="comment-text">
                                <div class="author-info">
                                    <h4><a href="#">{{$comment->creator->name}}</a></h4>
                                    <span class="reply" data-id="reply_box_{{$comment->id}}"><a href=#">{{trans('app.Reply')}}</a></span>
                                    <span class="comment-time">{{trans('app.Posted on')}} {{$comment->created_at}} /</span>
                                </div>
                                <p>{{$comment->content}}</p>
                                <div class="comment_box_reply" id="reply_box_{{$comment->id}}" style="display: none;">
                                    <div class="comments contact-form-area">
                                        @if(auth()->user())
                                        <form id="contact-form" action="{{lang()}}/comments/reply?reply_to={{$comment->id}}" method="post">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <textarea name="content" cols="30" rows="10" placeholder="{{trans('app.Message')}}"></textarea>
                                                    <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                                </div>
                                            </div>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($comment->replies()->latest()->get() as $c)
                        <div class="single-comment comment-reply">
                            <div class="author-image">
                                <img src="uploads/small/{{$c->creator->image}}" alt="{{$c->creator->name}}">
                            </div>
                            <div class="comment-text">
                                <div class="author-info">
                                    <h4><a href="#">{{$c->creator->name}}</a></h4>
                                    <span class="comment-time">{{trans('app.Posted on')}} {{$comment->created_at}}</span>
                                </div>
                                <p>{{$c->content}}</p>
                            </div>
                        </div>
                        @endforeach
                        @endforeach
                    </div>
                    @endif
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection


@push('js')
<script>
    $(function () {
        $('.reply').on('click', function () {
            var id = $(this).attr('data-id');
            $('#' + id).toggle();
            return false;
        });
    });
</script>
@endpush