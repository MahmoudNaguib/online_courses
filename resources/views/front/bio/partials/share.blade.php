<div class="social-links">
    <a href="https://facebook.com/sharer/sharer.php?u={{$row->link}}" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
    <a href="https://twitter.com/intent/tweet?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-twitter"></i></a>
    <a href="https://www.linkedin.com/shareArticle?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-linkedin"></i></a>
    <a href="https://web.whatsapp.com/send?text={{$row->link}}" target="_blank"><i class="zmdi zmdi-whatsapp"></i></a>
</div>