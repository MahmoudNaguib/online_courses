@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title mt-25">
                <h3>{{$page_title}}</h3>
                <p></p>
            </div>
        </div>
    </div>
</div>
<div class="course-details-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-12">
                <div class="course-details-content">
                    <div class="single-course-details">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="overlay-effect">
                                    <a href="#">
                                        <img src="uploads/large/{{$row->image}}" alt="{{$row->title}}">
                                    </a>
                                </div>
                            </div>  
                            <div class="col-md-6">
                                <div class="single-item-text">
                                    <div class="single-item-text-info">
                                        <span>{{trans('app.By')}}: <span>{{$row->creator->name}}</span></span>
                                        <span>{{trans('app.Date')}}: <span>{{str_limit($row->created_at,10,false)}}</span></span><br>
                                        <span>{{trans('app.Has certificate')}}: <span>{{($row->has_certificat)?trans('app.Yes'):trans('app.No')}}</span></span>
                                        <span>{{trans('app.Level')}}: <span>{{$row->level->title}}</span></span><br>
                                        <span>{{trans('app.Language')}}: <span>{{$row->language->title}}</span></span>
                                    </div>

                                    <div class="course-text-content">
                                        <p>{!! $row->content !!}</p>
                                        <p>
                                            <a href="{{lang()}}/orders/add/{{$row->id}}" class="btn btn-theme btn-success">{{trans('app.Buy')}}
                                            </a>
                                        </p>
                                        <div class="row news-details-content ">

                                            <div class="social-links">
                                                <span>{{trans('app.Share')}}:</span>
                                                <a href="https://facebook.com/sharer/sharer.php?u={{$row->link}}" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
                                                <a href="https://twitter.com/intent/tweet?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-twitter"></i></a>
                                                <a href="https://www.linkedin.com/shareArticle?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-linkedin"></i></a>
                                                <a href="https://web.whatsapp.com/send?text={{$row->link}}" target="_blank"><i class="zmdi zmdi-whatsapp"></i></a>
                                            </div>
                                        </div>
                                    </div>   

                                    @include('front.courses.counts') 
                                </div>
                            </div> 

                        </div>     
                    </div>
                    <div class="comments contact-form-area">
                        @if(auth()->user())
                        <h4 class="title">{{trans('app.Write your comment')}}</h4>
                        <form id="contact-form" action="{{lang()}}/comments?commentable_id={{$row->id}}&type=course" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea name="content" cols="30" rows="10" placeholder="{{trans('app.Message')}}"></textarea>
                                    <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                </div>
                            </div>
                        </form>
                        @else
                        <h4 class="title">{{trans('app.Need to write your comment')}} <a href="{{lang()}}/auth/login">{{trans('app.Login')}}</a></h4>
                        @endif
                    </div>
                    @php
                    $comments=$row->comments()->with(['replies'])->where('reply_to',NULL)->latest()->get();
                    @endphp
                    @if($comments)
                    <div class="comments">
                        <h4 class="title">{{trans('app.Comments')}}</h4>
                        @foreach($comments as $comment)
                        <div class="single-comment">
                            <div class="author-image">
                                <img src="uploads/small/{{$comment->creator->image}}" alt="{{$comment->creator->name}}-{{$comment->id}}">
                            </div>
                            <div class="comment-text">
                                <div class="author-info">
                                    <h4><a href="#">{{$comment->creator->name}}</a></h4>
                                    <span class="reply" data-id="reply_box_{{$comment->id}}"><a href=#">{{trans('app.Reply')}}</a></span>
                                    <span class="comment-time">{{trans('app.Posted on')}} {{$comment->created_at}} /</span>
                                </div>
                                <p>{{$comment->content}}</p>
                                <div class="comment_box_reply" id="reply_box_{{$comment->id}}" style="display: none;">
                                    <div class="comments contact-form-area">
                                        @if(auth()->user())
                                        <form id="contact-form" action="{{lang()}}/comments/reply?reply_to={{$comment->id}}" method="post">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <textarea name="content" cols="30" rows="10" placeholder="{{trans('app.Message')}}"></textarea>
                                                    <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                                </div>
                                            </div>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($comment->replies()->latest()->get() as $c)
                        <div class="single-comment comment-reply">
                            <div class="author-image">
                                <img src="uploads/small/{{$c->creator->image}}" alt="{{$c->creator->name}}">
                            </div>
                            <div class="comment-text">
                                <div class="author-info">
                                    <h4><a href="#">{{$c->creator->name}}</a></h4>
                                    <span class="comment-time">{{trans('app.Posted on')}} {{$comment->created_at}}</span>
                                </div>
                                <p>{{$c->content}}</p>
                            </div>
                        </div>
                        @endforeach
                        @endforeach
                    </div>
                    @endif
                </div>    
            </div>
            <div class="col-lg-3 col-md-12 col-12">
                <div class="sidebar-widget">
                    <div class="single-sidebar-widget">
                        <div class="tution-wrapper">
                            <div class="tution-fee">
                                <h1>{{$row->price}} <span class="currency">{{trans('app.EGP')}}</span></h1>
                            </div>
                            <div class="tutor-image">
                                <img src="uploads/small/{{$row->creator->image}}" alt="{{$row->creator->name}}">
                            </div>
                            <div class="single-teacher-text">
                                <h3><a href="{{lang()}}/bio/{{$row->created_by}}">{{$row->creator->name}}</a></h3>
                                <p>{{$row->creator->bio_limited}}</p>
                                <div class="single-item-comment-view">
                                    <span>{{$row->creator->courses()->active()->count()}} {{trans('app.Courses')}}</span>                          </div>
                                @if($rate=$row->creator->rate)
                                <div class="single-item-rating">
                                    @for($i=1; $i<=$rate; $i++)
                                    <i class="zmdi zmdi-star"></i>
                                    @endfor
                                    @for($i=1; $i<=5-$rate; $i++)
                                    <i class="zmdi zmdi-star-outline"></i>
                                    @endfor
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if($row->tags)
                    <div class="single-sidebar-widget">
                        <h4 class="title">{{trans('app.Search by Tags')}}</h4>
                        <ul class="tags">
                            @foreach(explode(',',$row->tags) as $tag)
                            <li><a href="{{lang()}}/courses?tags={{$tag}}">{{$tag}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if($relatedCourses)
                    <div class="single-sidebar-widget">
                        <h4 class="title">{{trans('app.Related Courses')}}</h4>
                        @foreach($relatedCourses as $item)
                        <div class="single-item">
                            <div class="single-item-image overlay-effect">
                                <a href="{{$item->link}}">
                                    <img alt="{{$item->title}}" src="uploads/small/{{$item->image}}">
                                </a>
                            </div>
                            <div class="single-item-text">
                                <h4><a href="{{$item->link}}">{{$item->title_limited}}</a></h4>
                                <div class="single-item-text-info">
                                    <span>{{trans('app.By')}}: {{$item->creator->name}}<span></span></span>
                                    <span>{{trans('app.Date')}}: <span>{{str_limit($item->created_at,10,false)}}</span></span>
                                </div>
                                <p>{{$item->content_limited}}</p>
                                @include('front.courses.counts',['row'=>$item])
                            </div>
                            <div class="button-bottom">
                                <a href="{{lang()}}/orders/add/{{$item->id}}" class="btn btn-theme btn-success">{{trans('Buy')}}
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('js')
<script>
    $(function () {
        $('.reply').on('click', function () {
            var id = $(this).attr('data-id');
            $('#' + id).toggle();
            return false;
        });
    });
</script>
@endpush