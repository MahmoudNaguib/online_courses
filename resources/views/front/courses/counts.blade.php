<div class="single-item-content">
    <div class="single-item-comment-view">
        <span><i class="zmdi zmdi-eye"></i>{{$row->views}}</span>
        <span><i class="zmdi zmdi-comments"></i>{{$row->comments->count()}}</span>
        <span>
            <a href="favourites/add/{{$row->id}}"><i class="zmdi zmdi-favorite"></i></a>
        </span>
    </div>
    @if($row->rate)
    <div class="single-item-rating">
        @for($i=1; $i<=$row->rate; $i++)
        <i class="zmdi zmdi-star"></i>
        @endfor
        @for($i=1; $i<=5-$row->rate; $i++)
        <i class="zmdi zmdi-star-outline"></i>
        @endfor
    </div>
    @endif
</div>  
