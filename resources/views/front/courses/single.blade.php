<div class="col-lg-4 col-md-6 col-12">
    <div class="single-item">
        <div class="single-item-image overlay-effect">
            <a href="{{$row->link}}">
                <img src="uploads/small/{{$row->image}}" alt="{{$row->title}}">
            </a>
        </div>
        <div class="single-item-text">
            <h4><a href="{{$row->link}}">{{$row->title}}</a></h4>
            <div class="single-item-text-info">
                <span>{{trans('app.By')}}: <span>{{$row->creator->name}}</span></span>
                <span>{{trans('app.Date')}}: <span>{{$row->created_at->format('F j, Y')}}</span></span>
            </div>
             <div class="single-item-comment-view">
                <span><i class="zmdi zmdi-settings"></i><a href="{{lang()}}/courses?section_id={{$row->section_id}}">{{$row->section->title}}</a></span>
            </div>
            <p>{{$row->content_limited}}</p>
            @include('front.courses.counts')
        </div>
        <div class="button-bottom">
            {!! $row->order_button !!}
        </div>
    </div>
</div>