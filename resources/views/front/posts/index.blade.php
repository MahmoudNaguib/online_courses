@extends('front.layouts.master')

@section('content')
<div class="latest-area section-padding bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-40 mb-40">
            @if (!$rows->isEmpty())
            @foreach ($rows as $row)
            @include('front.posts.single')
            @endforeach
            <div class="container">
                <div class="paganition-center center">
                    {!! $rows->appends(['created_by'=>request('created_by'),'category_id'=>request('category_id')])->render() !!}
                </div>
            </div>
            @else
            {{trans("app.There is no results")}}
            @endif
        </div>
    </div>
</div>
@endsection
