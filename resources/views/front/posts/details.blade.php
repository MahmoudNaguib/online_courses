@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title">
                <h3>{{$page_title}}</h3>
                <p></p>
            </div>
        </div>
    </div>
</div>
<!--News Details Area Start-->
<div class="news-details-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-12">
                <div class="news-details-content">
                    <div class="single-latest-item">
                        @if($row->image)
                        <div class="col-md-6 offset-md-3 text-center">
                            <img src="uploads/large/{{$row->image}}" alt="{{$row->title}}" class="img-fluid">
                        </div>
                        @endif
                        <div class="single-latest-text">
                            <div class="single-item-comment-view">
                                <span><i class="zmdi zmdi-calendar-check"></i>{{$row->created_at->format('F j, Y')}}</span>
                                <span><i class="zmdi zmdi-eye"></i>{{$row->views}}</span>
                                <span><i class="zmdi zmdi-comments"></i>{{$row->comments->count()}}</span>
                            </div>
                            <p>{!! $row->content !!}</p>
                            <div class="tags-and-links">
                                @if($row->tags)
                                <div class="related-tag">
                                    <span>{{trans('app.Tags')}}:</span>
                                    <ul class="tags">
                                        @foreach(explode(',',$row->tags) as $tag)
                                        <li><a href="{{lang()}}/posts?tags={{$tag}}">{{$tag}},</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <div class="social-links">
                                    <span>{{trans('app.Share')}}:</span>
                                    <a href="https://facebook.com/sharer/sharer.php?u={{$row->link}}" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
                                    <a href="https://twitter.com/intent/tweet?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-twitter"></i></a>
                                    <a href="https://www.linkedin.com/shareArticle?url={{$row->link}}" target="_blank"><i class="zmdi zmdi-linkedin"></i></a>
                                    <a href="https://web.whatsapp.com/send?text={{$row->link}}" target="_blank"><i class="zmdi zmdi-whatsapp"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="comments contact-form-area">
                        @if(auth()->user())
                        <h4 class="title">{{trans('app.Write your comment')}}</h4>
                        <form id="contact-form" action="{{lang()}}/comments?commentable_id={{$row->id}}&type=post" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea name="content" cols="30" rows="10" placeholder="{{trans('app.Message')}}"></textarea>
                                    <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                </div>
                            </div>
                        </form>
                        @else
                        <h4 class="title">{{trans('app.Need to write your comment')}} <a href="{{lang()}}/auth/login">{{trans('app.Login')}}</a></h4>
                        @endif
                    </div>
                    @php
                    $comments=$row->comments()->with(['replies'])->where('reply_to',NULL)->latest()->get();
                    @endphp
                    @if($comments)
                    <div class="comments">
                        <h4 class="title">{{trans('app.Comments')}}</h4>
                        @foreach($comments as $comment)
                        <div class="single-comment">
                            <div class="author-image">
                                <img src="uploads/small/{{$comment->creator->image}}" alt="{{$comment->creator->name}}-{{$comment->id}}">
                            </div>
                            <div class="comment-text">
                                <div class="author-info">
                                    <h4><a href="#">{{$comment->creator->name}}</a></h4>
                                    <span class="reply" data-id="reply_box_{{$comment->id}}"><a href=#">{{trans('app.Reply')}}</a></span>
                                    <span class="comment-time">{{trans('app.Posted on')}} {{$comment->created_at}} /</span>
                                </div>
                                <p>{{$comment->content}}</p>
                                <div class="comment_box_reply" id="reply_box_{{$comment->id}}" style="display: none;">
                                    <div class="comments contact-form-area">
                                        @if(auth()->user())
                                        <form id="contact-form" action="{{lang()}}/comments/reply?reply_to={{$comment->id}}" method="post">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <textarea name="content" cols="30" rows="10" placeholder="{{trans('app.Message')}}"></textarea>
                                                    <button type="submit" class="button-default">{{trans('app.SUBMIT')}}</button>
                                                </div>
                                            </div>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($comment->replies()->latest()->get() as $c)
                        <div class="single-comment comment-reply">
                            <div class="author-image">
                                <img src="uploads/small/{{$c->creator->image}}" alt="{{$c->creator->name}}">
                            </div>
                            <div class="comment-text">
                                <div class="author-info">
                                    <h4><a href="#">{{$c->creator->name}}</a></h4>
                                    <span class="comment-time">{{trans('app.Posted on')}} {{$comment->created_at}}</span>
                                </div>
                                <p>{{$c->content}}</p>
                            </div>
                        </div>
                        @endforeach
                        @endforeach
                    </div>
                    @endif
                </div>    
            </div>
            <div class="col-lg-3 col-md-12 col-12">
                <div class="sidebar-widget">
                    @if($recentPosts)
                    <div class="single-sidebar-widget">
                        <h4 class="title">{{trans('app.Recent posts')}}</h4>
                        @foreach($recentPosts as $recentPost)
                        <div class="recent-content">
                            <div class="recent-content-item">
                                <a href="#">
                                    <img src="uploads/small/{{$recentPost->image}}" alt="{{$recentPost->title}}" width='50'>
                                </a>
                                <div class="recent-text">
                                    <h4><a href="{{$recentPost->link}}">{{$recentPost->title_limited}}</a></h4>
                                    <div class="single-item-comment-view">
                                        <span><i class="zmdi zmdi-eye"></i>{{$recentPost->views}}</span>
                                        <span><i class="zmdi zmdi-comments"></i>{{$recentPost->comments->count()}}</span>
                                    </div>
                                    <p>{{$recentPost->content_limited}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                    @php
                    $comments=$row->comments()->latest()->take(3)->get();
                    @endphp
                    @if($comments)
                    <div class="single-sidebar-widget comment">
                        <h4 class="title">{{trans('app.Recent Comments')}}</h4>
                        <div class="recent-content">
                            @foreach($comments as $comment)
                            <div class="recent-content-item">
                                <a href="#"><img src="uploads/small/{{$comment->creator->image}}" alt="{{$comment->creator->name}}" width="50"></a>
                                <div class="recent-text">
                                    <h4>{{$comment->creator->name}}</h4>
                                    <p>{{$comment->content_limited}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                    @if($row->tags)
                    <div class="related-tag">
                        <span>{{trans('app.Tags')}}:</span>
                        <ul class="tags">
                            @foreach(explode(',',$row->tags) as $tag)
                            <li><a href="{{lang()}}/posts?q={{$tag}}">{{$tag}},</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if($row->tags)
                    <div class="single-sidebar-widget">
                        <h4 class="title">{{trans('app.Search by Tags')}}</h4>
                        <ul class="tags">
                            @foreach(explode(',',$row->tags) as $tag)
                            <li><a href="{{lang()}}/posts?q={{$tag}}">{{$tag}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('js')
    <script>
        $(function () {
            $('.reply').on('click', function () {
                var id = $(this).attr('data-id');
                $('#' + id).toggle();
                return false;
            });
        });
    </script>
    @endpush