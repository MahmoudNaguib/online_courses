@extends('layouts.auth')
@section('content')
<div class="signin-box">
    <h3 class="signin-title-secondary">{{$page_title}}</h3>
    {!! Form::open(['method' => 'post'] ) !!}
    {{ csrf_field() }}
    <div class="form-group">
        @php $input = 'email';
        @endphp {!! Form::text($input,request($input),['class'=>'form-control','required'=>'required','placeholder'=>trans('auth.Enter your email')]) !!} 
        @if(@$errors)
        @foreach($errors->get($input) as $message)
        <span class='help-inline text-danger'>{{ $message }}</span>
        @endforeach 
        @endif
    </div>
    <!-- form-group -->
    <button class="btn btn-primary btn-block btn-signin">{{ trans('auth.Submit') }}</button>
    <p class="mg-b-0">{{trans('auth.Do you have account')}}? <a href="auth/login">{{ trans('auth.Login') }}</a></p>
    {!! Form::close() !!}
</div>
@endsection
