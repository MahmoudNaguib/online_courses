@extends('emails.master')

@section('title'){{trans('app.Reminder From') . " " . appName() }} @endsection

@section('content')
<h2>{{trans("email.Reminder Has Been Received")}}</h2>
<p>
    <label>
        <strong>{{trans("email.Dear")}} </strong>
        {{ \App\Models\User::find($row->user_id)->name }} ,
    </label>
    <br>
<p>
    {{$row->subject }}
</p>
<br>
<a href="{{ url('reminders/view-own/'.$row->id) }}">{{ trans('email.Check it') }}</a>
</p>
@endsection
