<div class="row mg-t-20">
    <label class="col-sm-4"><b>{{ @$attributes['label'] }}</b></label>
    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        @if(isset($attributes['type']))
            @if(@$attributes['type'] == 'image' )
                {!!image(@$attributes['value'],'small') !!}
            @else
               {!! file(@$attributes['value']) !!}
            @endif
        @else
        {{@$attributes['value']}}
        @endif
    </div>
</div>
