<?php 

return [
    '0' => '1',
    'Unit' => 'Unit',
    'Select Unit' => 'Select Unit',
    'Title' => 'Title',
    'Content' => 'Content',
    'Video' => 'Video',
    'Is active' => 'Is active',
    'Attachment' => 'Attachment',
];