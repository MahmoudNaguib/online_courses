<?php 

return [
    '0' => '1',
    'Password' => 'Password',
    'Password confirmation' => 'Password confirmation',
    'Save' => 'Save',
    'Name' => 'Name',
    'Email' => 'Email',
    'Mobile' => 'Mobile',
    'Default language' => 'Default language',
    'Bio' => 'Bio',
    'Meta description' => 'Meta description',
    'Meta keywords' => 'Meta keywords',
    'Image' => 'Image',
    'Edit Profile' => 'Edit Profile',
    'Change password' => 'Change password',
];