<?php 

return [
    '0' => '1',
    'ID' => 'ID',
    'Type' => 'Type',
    'Post' => 'Post',
    'Course' => 'Course',
    'Created by' => 'Created by',
    'Created at' => 'Created at',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'User' => 'User',
    'Title' => 'Title',
];