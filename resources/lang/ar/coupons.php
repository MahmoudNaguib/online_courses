<?php 

return [
    '0' => '1',
    'ID' => 'ID',
    'Course' => 'Course',
    'Coupon' => 'Coupon',
    'Discount' => 'Discount',
    'Expiry date' => 'Expiry date',
    'Total' => 'Total',
    'Created at' => 'Created at',
    'Edit' => 'Edit',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Select Course' => 'Select Course',
    'Code' => 'Code',
];