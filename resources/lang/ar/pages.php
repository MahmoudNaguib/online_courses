<?php 

return [
    '0' => '1',
    'About us' => 'About us',
    'Privacy policy' => 'Privacy policy',
    'Terms and conditions' => 'Terms and conditions',
    'Title' => 'Title',
    'Content' => 'Content',
    'Tags' => 'Tags',
    'Meta description' => 'Meta description',
    'Facebook pixel' => 'Facebook pixel',
    'Image' => 'Image',
    'Is active' => 'Is active',
    'ID' => 'ID',
    'Views count' => 'Views count',
    'Active' => 'Active',
    'Created by' => 'Created by',
    'Created at' => 'Created at',
    'Edit' => 'Edit',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Slug' => 'Slug',
    'Meta keywords' => 'Meta keywords',
];