<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LikesControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_likes() {
        dump('test_list_likes');
        $user = \App\Models\User::where('type','admin')->first();
        $latest = \App\Models\Like::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/likes')
                ->assertStatus(200)
                ->assertSee('likes');
    }


    public function test_delete_likes() {
        dump('test_delete_likes');
        $user = \App\Models\User::where('type','admin')->first();
        $record = \App\Models\Like::create(factory(\App\Models\Like::class)->make()->toArray());
        $row = factory(\App\Models\Like::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/likes/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_likes() {
        dump('view_delete_likes');
        $user = \App\Models\User::where('type','admin')->first();
        $record = \App\Models\Like::create(factory(\App\Models\Like::class)->make()->toArray());
        $row = factory(\App\Models\Like::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/likes/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
