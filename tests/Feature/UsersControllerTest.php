<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_users() {
        dump('test_list_users');
        $user = \App\Models\User::where('type','admin')->first();
        $latest = \App\Models\User::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/users')
                ->assertStatus(200)
                ->assertSee('users');
    }

    public function test_create_users() {
        dump('test_create_users');
        $user = \App\Models\User::where('type','admin')->first();
        $row = factory(\App\Models\User::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/users/create', array_merge($row->toArray(), ['password' => '12345678', 'password_confirmation' => '12345678']));
        $latest = \App\Models\User::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_users() {
        dump('test_edit_users');
        $user = \App\Models\User::where('type','admin')->first();
        $record = \App\Models\User::create(factory(\App\Models\User::class)->make()->toArray());
        $row = factory(\App\Models\User::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/users/edit/' . $record->id, $row->toArray());
        $record = \App\Models\User::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_users() {
        dump('test_delete_users');
        $user = \App\Models\User::where('type','admin')->first();
        $record = \App\Models\User::create(factory(\App\Models\User::class)->make()->toArray());
        $row = factory(\App\Models\User::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/users/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

}
