<?php
AdvancedRoute::controller('auth', 'AuthController');
AdvancedRoute::controller('profile', 'ProfileController');
AdvancedRoute::controller('notifications', 'NotificationsController');
AdvancedRoute::controller('roles', 'RolesController');
AdvancedRoute::controller('countries', 'CountriesController');
AdvancedRoute::controller('currencies', 'CurrenciesController');
AdvancedRoute::controller('users', 'UsersController');
AdvancedRoute::controller('posts', 'PostsController');
AdvancedRoute::controller('dashboard', 'DashBoardController');
AdvancedRoute::controller('events', 'EventsController');
AdvancedRoute::controller('groups', 'GroupsController');
AdvancedRoute::controller('programs', 'ProgramsController');
AdvancedRoute::controller('items', 'ItemsController');
AdvancedRoute::controller('contacts', 'ContactsController');
AdvancedRoute::controller('children', 'ChildrenController');
AdvancedRoute::controller('/', 'DashBoardController');
