<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* Route::get('/', function () {
  return view('welcome');
  });
 */
Route::group(['prefix' => (app()->environment() == 'testing') ? 'en' : LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function() {
    AdvancedRoute::controller('auth', 'AuthController');
    Route::group(['middleware' => ['auth']], function() {
        AdvancedRoute::controller('profile', 'ProfileController');
        AdvancedRoute::controller('notifications', 'NotificationsController');
        Route::group(['prefix' => 'admin'], function() {
            Route::group(['middleware' => ['isAdmin']], function() {
                AdvancedRoute::controller('roles', 'Admin\RolesController');
                AdvancedRoute::controller('users', 'Admin\UsersController');
                AdvancedRoute::controller('posts', 'Admin\PostsController');
                AdvancedRoute::controller('pages', 'Admin\PagesController');
                AdvancedRoute::controller('categories', 'Admin\CategoriesController');
                AdvancedRoute::controller('comments', 'Admin\CommentsController');
                AdvancedRoute::controller('likes', 'Admin\LikesController');
                AdvancedRoute::controller('rates', 'Admin\RatesController');
                AdvancedRoute::controller('configs', 'Admin\ConfigsController');
                AdvancedRoute::controller('search', 'Admin\SearchController');
                AdvancedRoute::controller('translator', 'Admin\TranslatorController');
                AdvancedRoute::controller('options', 'Admin\OptionsController');
                AdvancedRoute::controller('messages', 'Admin\MessagesController');
                AdvancedRoute::controller('sections', 'Admin\SectionsController');
                AdvancedRoute::controller('courses', 'Admin\CoursesController');
                AdvancedRoute::controller('contacts', 'Admin\ContactsController');
                AdvancedRoute::controller('units', 'Admin\UnitsController');
                AdvancedRoute::controller('lectures', 'Admin\LecturesController');
                AdvancedRoute::controller('menu', 'Admin\MenuController');
                AdvancedRoute::controller('ajax', 'Admin\AjaxController');
            });
            AdvancedRoute::controller('/', 'Admin\DashBoardController');
        });
        Route::group(['middleware' => ['isTeacher'], 'prefix' => 'teacher'], function() {
            AdvancedRoute::controller('/', 'Teacher\DashBoardController');
            AdvancedRoute::controller('courses', 'Teacher\CoursesController');
            AdvancedRoute::controller('coupons', 'Teacher\CouponsController');
            AdvancedRoute::controller('comments', 'Teacher\CommentsController');
            AdvancedRoute::controller('likes', 'Teacher\LikesController');
            AdvancedRoute::controller('rates', 'Teacher\RatesController');
            AdvancedRoute::controller('units', 'Teacher\UnitsController');
            AdvancedRoute::controller('lectures', 'Teacher\LecturesController');
        });
        AdvancedRoute::controller('/', 'Teacher\DashBoardController');
        AdvancedRoute::controller('dashboard', 'DashboardController');
        AdvancedRoute::controller('comments', 'CommentsController');
        AdvancedRoute::controller('ajax', 'AjaxController');
    });
    AdvancedRoute::controller('/orders', 'OrdersController');
    AdvancedRoute::controller('/learn', 'LearnController');
    AdvancedRoute::controller('/favourites', 'FavouritesController');
    AdvancedRoute::controller('/courses', 'CoursesController');
    AdvancedRoute::controller('/posts', 'PostsController');
    AdvancedRoute::controller('/bio', 'BioController');
    AdvancedRoute::controller('/messages', 'MessagesController');
    AdvancedRoute::controller('/contacts', 'ContactsController');
    AdvancedRoute::controller('/pages', 'PagesController');
    Route::get('/about', 'PagesController@getAbout');
    Route::get('/contact', 'PagesController@getContact');
    AdvancedRoute::controller('/', 'HomeController');
});


Route::prefix('api')->group(function () {
    require_once __DIR__ . '/api.php';
});
