<?php

return[
    'admin\users' => ['edit', 'view', 'delete', 'create'],
    'admin\categories' => ['create', 'edit', 'view', 'delete'],
    'admin\posts' => ['create', 'edit', 'view', 'delete'],
    'admin\options' => ['create', 'edit', 'view', 'delete'],
    'admin\sections' => ['create', 'edit', 'view', 'delete'],
    'admin\courses' => ['create', 'edit', 'view', 'delete'],
    'admin\pages' => ['create', 'edit', 'view', 'delete'],
    'admin\menu' => ['edit', 'view', 'delete', 'create'],
    'admin\lectures' => ['create', 'edit', 'delete'],
    'admin\units' => ['create', 'edit', 'delete'],
    'admin\comments' => ['view', 'delete'],
    'admin\likes' => ['view', 'delete'],
    'admin\rates' => ['view', 'delete'],
    'admin\translator' => ['edit', 'view'],
];
