<?php
use Faker\Generator as Faker;
/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Lecture::class, function (Faker $faker) {
    $record="lecture 1 ".rand(1,1000);
    foreach(langs() as $lang) {
        $title[$lang]=$record;
    }
    $unitsIds = App\Models\Unit::active()->pluck('id')->toArray();
    $unit=  \App\Models\Unit::find($unitsIds[array_rand($unitsIds)]);
    
    return [
        'course_id' => $unit->course_id,
        'unit_id' => $unit->id,
        'title'=>$title,
        'created_by'=>1,
        'order_field'=>rand(1,100),
        'is_active'=>1
    ];
});
