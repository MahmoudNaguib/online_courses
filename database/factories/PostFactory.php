<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Post::class, function (Faker $faker) {
    foreach (langs() as $lang) {
        $title[$lang] = $faker->sentence(4);
        $slug[$lang] = slug($title[$lang]);
        $content[$lang] = $faker->paragraph();
    }
    $categoriesIds = App\Models\Category::pluck('id')->toArray();
    return [
        'category_id' => $categoriesIds[array_rand($categoriesIds)],
        'title' => $title,
        'slug' => $slug,
        'content' => $content,
        'is_active' => 1,
        'created_by' => 2,
        'views'=>rand(1,1000)
    ];
});
