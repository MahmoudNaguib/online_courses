<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Favourite::class, function (Faker $faker) {
    $userIds = App\Models\User::pluck('id')->toArray();
    $courseIds = App\Models\Course::pluck('id')->toArray();
    return [
        'user_id' => $userIds[array_rand($userIds)],
        'course_id' => $courseIds[array_rand($courseIds)],
    ];
});
