<?php
use Faker\Generator as Faker;
/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Category::class, function (Faker $faker) {
    $record="Category ".rand(1,1000);
    foreach(langs() as $lang) {
        $title[$lang]=$record;
    }
    return [
        'title'=>$title,
        'created_by'=>1,
        'is_active'=>1
    ];
});
