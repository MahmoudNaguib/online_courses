<?php
use Faker\Generator as Faker;
/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Unit::class, function (Faker $faker) {
    $record="Unit ".rand(1,1000);
    foreach(langs() as $lang) {
        $title[$lang]=$record;
    }
    $coursesIds = App\Models\Course::active()->pluck('id')->toArray();
    return [
        'course_id' => $coursesIds[array_rand($coursesIds)],
        'title'=>$title,
        'created_by'=>1,
        'order_field'=>rand(1,100),
        'is_active'=>1
    ];
});
