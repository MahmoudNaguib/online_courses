<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */
$factory->define(App\Models\Config::class, function (Faker $faker) {
    foreach (langs() as $lang) {
        $record[$lang] = $faker->sentence(3);
    }
    return [
        'type' => 'Basic Information',
        'field' => 'field_' . str_random(10),
        'value' => json_encode($record),
        'created_by' => 2,
    ];
});
