<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Rate::class, function (Faker $faker) {
    $types = ['course','user'];
    $type = $types[array_rand($types)];
    if ($type == 'user') {
        $itemIds = App\Models\User::IsTeacher()->pluck('id')->toArray();
        $itemType = 'App\Models\User';
    }
    else {
        $itemIds = App\Models\Course::pluck('id')->toArray();
        $itemType = 'App\Models\Course';
    }
    $usersIds = App\Models\User::pluck('id')->toArray();
    return [
        'rateable_id' => $itemIds[array_rand($itemIds)],
        'rateable_type' => $itemType,
        'created_by' => $usersIds[array_rand($usersIds)],
        'value'=>rand(1,5)
    ];
});
