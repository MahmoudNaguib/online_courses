<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName . ' ' . $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'mobile' => '0122' . rand(1111111, 9999999),
        'password' => '12345678', // secret
        'confirmed' => 1,
        'is_active' => 1,
        'is_teacher' => rand(0,1),
        'language' => 'en',
        'created_by' => 2,
    ];
});
