<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('slug')->nullable();
            $table->bigInteger('section_id')->nullable()->index()->default(1); //required
            $table->float('price', 11, 2)->index()->default(0);
            $table->text('title')->nullable();  //translatable
            $table->text('content')->nullable(); //translatable
            $table->bigInteger('level_id')->nullable(); 
            $table->bigInteger('language_id')->nullable();
            $table->boolean('has_certificate')->nullable()->default(0)->index();
            $table->text('meta_description')->nullable(); //translatable
            $table->text('meta_keywords')->nullable(); // translatable
            $table->text('facebook_pixel')->nullable();
            $table->text('tags')->nullable();  //translatable
            $table->string('image')->nullable();
            $table->boolean('is_active')->nullable()->default(0)->index();
            $table->boolean('admin_active')->nullable()->default(0)->index();
            $table->bigInteger('views')->nullable()->index()->default(0);
            $table->bigInteger('created_by')->nullable()->index();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('posts');
    }
}