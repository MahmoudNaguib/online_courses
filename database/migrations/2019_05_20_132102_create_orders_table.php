<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('created_by')->nullable()->index()->default(NULL);
            $table->bigInteger('course_id')->nullable()->index()->default(NULL);
            $table->float('price', 11, 2)->index()->default(0);
            $table->float('discount', 11, 2)->index()->default(0);
            $table->float('total', 11, 2)->index()->default(0);
            $table->bigInteger('coupon_id')->nullable()->index()->default(NULL);
            $table->boolean('confirmed')->nullable()->default(0)->index();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('orders');
    }

}
