<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable()->index();
            $table->bigInteger('course_id')->nullable()->index()->default(NULL);
            $table->float('discount', 11, 2)->index()->default(0);
            $table->date('expiry_date')->nullable();
            $table->bigInteger('total')->index()->default(0);
            $table->bigInteger('created_by')->nullable()->index()->default(NULL);
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('coupons');
    }

}
