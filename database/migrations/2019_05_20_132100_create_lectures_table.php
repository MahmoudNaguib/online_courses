<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('lectures', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('course_id')->nullable()->index()->default(NULL);
            $table->bigInteger('unit_id')->nullable()->index()->default(NULL);
            $table->text('title')->nullable();  //required
            $table->text('content')->nullable();
            $table->string('url')->nullable();
            $table->string('video')->nullable();
            $table->string('attachment')->nullable();
            $table->boolean('is_active')->nullable()->default(1)->index();
            $table->bigInteger('views')->nullable()->index()->default(0);
            $table->bigInteger('order_field')->nullable()->index()->default(0);
            $table->bigInteger('created_by')->nullable()->index();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('lectures');
    }

}
