<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(BasicSeeder::class);
        if (app()->environment() != 'production') {
            $this->call(NotificationsSeeder::class);
            $this->call(CategoriesSeeder::class);
            $this->call(PostsSeeder::class);
            $this->call(SectionsSeeder::class);            
            $this->call(CoursesSeeder::class);
            $this->call(UnitsSeeder::class);
            $this->call(LecturesSeeder::class);
            $this->call(CommentsSeeder::class);
            $this->call(LikesSeeder::class);
            $this->call(RatesSeeder::class);
            $this->call(FavouritesSeeder::class);
        }
    }

}
