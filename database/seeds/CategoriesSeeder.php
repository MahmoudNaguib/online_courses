<?php
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('categories')->delete();
            DB::statement("ALTER TABLE categories AUTO_INCREMENT = 1");
            factory(App\Models\Category::class, 5)->create();
        }
    }
}