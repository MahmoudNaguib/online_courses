<?php

use Illuminate\Database\Seeder;

class NotificationsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            DB::table('notifications')->delete();
            DB::statement("ALTER TABLE notifications AUTO_INCREMENT = 1");
            factory(App\Models\Notification::class, 10)->create();
        }
    }

}
