<?php
use Illuminate\Database\Seeder;

class UnitsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('units')->delete();
            DB::statement("ALTER TABLE units AUTO_INCREMENT = 1");
            factory(App\Models\Unit::class, 20)->create();
        }
    }
}