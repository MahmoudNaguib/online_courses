<?php
use Illuminate\Database\Seeder;

class LecturesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('lectures')->delete();
            DB::statement("ALTER TABLE lectures AUTO_INCREMENT = 1");
            factory(App\Models\Lecture::class,40)->create();
        }
    }
}