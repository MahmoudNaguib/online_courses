<?php
use Illuminate\Database\Seeder;

class LikesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('likes')->delete();
            DB::statement("ALTER TABLE likes AUTO_INCREMENT = 1");
            factory(App\Models\Like::class, 100)->create();
        }
    }
}