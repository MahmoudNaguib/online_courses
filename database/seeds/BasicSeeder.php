<?php
use Illuminate\Database\Seeder;

class BasicSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        ///////////////////////////////////////////////////////////////// Default Configs
        DB::table('configs')->delete();
        DB::statement("ALTER TABLE configs AUTO_INCREMENT = 1");
        insertDefaultConfigs();
        ////////////////////////////////////////////////////////// insert default role
        DB::table('roles')->delete();
        DB::statement("ALTER TABLE roles AUTO_INCREMENT = 1");
        insertDefaultRoles();
        //////////////////////////////////////////////////// Insert users
        DB::table('users')->delete();
        DB::statement("ALTER TABLE users AUTO_INCREMENT = 1");
        insertDefaultUsers();
        ////////////////////////////////////////////////////////// insert in tokens
        DB::table('tokens')->delete();
        DB::statement("ALTER TABLE tokens AUTO_INCREMENT = 1");
        insertDefaultTokens();
        ///////////////////////////////////// Options
        DB::table('options')->delete();
        DB::statement("ALTER TABLE options AUTO_INCREMENT = 1");
        insertDefaultOptions();
        /////////////////////////////////////////////////////////// Other Seeds
    }
}