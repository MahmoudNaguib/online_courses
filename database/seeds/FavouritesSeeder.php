<?php
use Illuminate\Database\Seeder;

class FavouritesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('favourites')->delete();
            DB::statement("ALTER TABLE favourites AUTO_INCREMENT = 1");
            factory(App\Models\Favourite::class, 10)->create();
        }
    }
}