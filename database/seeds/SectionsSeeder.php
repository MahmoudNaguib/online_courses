<?php
use Illuminate\Database\Seeder;

class SectionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('sections')->delete();
            DB::statement("ALTER TABLE sections AUTO_INCREMENT = 1");
            factory(App\Models\Section::class,5)->create();
            insertDummyChildSections();
        }
    }
}