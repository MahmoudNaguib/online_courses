<?php

use Illuminate\Database\Seeder;

class RatesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            DB::table('rates')->delete();
            DB::statement("ALTER TABLE rates AUTO_INCREMENT = 1");
            factory(App\Models\Rate::class, 100)->create();
        }
    }

}
