<?php
use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('pages')->delete();
            DB::statement("ALTER TABLE pages AUTO_INCREMENT = 1");
            factory(App\Models\Page::class, 3)->create();
        }
    }
}